#!/usr/bin/python
import pygame.midi, time, liblo, sys

from_launchpad = None
to_launchpad = None

debug = False

inport = 8010
outport = 8011
color_ids = {'off': 12,
             'red1': 13, 'red2': 14, 'red3': 15, 'red': 15,
             'green': 60,
             'yellow1': 29,'yellow2': 63,'yellow3': 62,'yellow': 62,
             'clearall': -1
             }



def midi_listen():
    event_get = pygame.fastevent.get
    event_post = pygame.fastevent.post
    

    while True:
        server.recv(10)
        events = event_get()
        if from_launchpad.poll():
            midi_events = from_launchpad.read(10)
            for event in midi_events:
                event = event[0]
                if debug:
                    print event
                if len(event) == 4:
                    pressed = event[2] / 127
                    row = event[1] / 16
                    col = event[1] % 16
                    if event[0] == 176:
                        row = -1
                        col = col - 8
                    msg = liblo.Message("/launchpad")
                    msg.add(row,col,pressed)
                    liblo.send(osc_target,msg)



def init():
    global input_id
    global inport, outport
    global osc_target
    global server
    global debug

    osc_target = liblo.Address(outport)
    pygame.init()
    pygame.midi.init()
    pygame.fastevent.init()
    input_id = pygame.midi.get_default_input_id()

    server = start_server(inport)
    server.add_method(None, None, send_to_launchpad)

    


def color_from_name(name):
    global color_ids
    try:
        return color_ids[name]
    except:
        return 0

def send_to_launchpad(path, args, type):
    if path == '/launchpad' and len(args) == 3:
        row = args[0]
        col = args[1]
        if row < -1 or row > 7 or col < 0 or col > 8 or (row == -1 and col == 8):
            return
        color = color_from_name(args[2])
        status = 176
        if color == -1:
            if debug:
                print "clear all"
            to_launchpad.write([[[status, 0, 0],0]])            
            to_launchpad.write([[[status, 0, 1],0]])
            return
        elif row == -1:
            button = 104 + col
        else:
            status = 144
            button = row * 16 + col
        if debug:
            print "button:" + str(button) + ", color" + str(color)
        to_launchpad.write([[[status, button, color],0]])


def start_server(port):
    try:
        return liblo.Server(port)
    except liblo.ServerError, err:
        print str(err)
        sys.exit()



def cleanup():
    global from_launchpad, to_launchpad
    pygame.midi.quit()
    sys.exit()


def connect_launchpad():
    global from_launchpad, to_launchpad

    ids = range( pygame.midi.get_count())
    for i in ids:
        (interf, name, input, output, opened) = pygame.midi.get_device_info(i)
        if 'Launchpad' in name and input:
            from_launchpad = pygame.midi.Input(i, 0)
            if debug:
                print "connected to launchpad input:" + name
        if 'Launchpad' in name and output:
            to_launchpad = pygame.midi.Output(i, 0)
            if debug:
                print "connected to launchpad output:" + name

    if not from_launchpad or not to_launchpad:
        sys.exit()




init()
connect_launchpad()
midi_listen()
del from_launchpad
del to_launchpad
cleanup()


