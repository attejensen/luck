public class Launchpad {
    1 => static int debug;

    "localhost" => string hostname;
    8010 => int in_port;
    8011 => int out_port;
    new OscSend @=> static OscSend @ osc_out;
    osc_out.setHost(hostname, out_port);


    new OscRecv @=> static OscRecv @ osc_in;
    in_port => osc_in.port;
    osc_in.listen();


    1 => static int default_channel;
    
    new MidiMsg @=> static MidiMsg @ midimessage;
    new MidiOut @=> static MidiOut @ launchpad_out;
    new MidiIn @=> static MidiIn @ launchpad_in;

    104 => static int button_up;
    105 => static int button_down;
    106 => static int button_left;
    107 => static int button_right;
    108 => static int button_session;
    109 => static int button_user1;
    110 => static int button_user2;
    111 => static int button_mixer;

    [12,13,14,15,60,29,63,62] @=> static int colors[];
    /*
    0 = off
    1 = light read
    2 = medium red
    3 = bright red
    4 = green
    5 = dark yellow
    6 = medium yellow
    7 = bright yellow
    */
    
    connect_by_name("Launchpad MIDI 1");

    clear();
    
    spork ~ midi_listen();
    spork ~ osc_listen();

    public static void osc(int row, int col, int pressed){
        osc_out.startMsg("/launchpad, i, i, i");
        row => osc_out.addInt;
        col => osc_out.addInt;
        pressed => osc_out.addInt;
    }

    

    public static void clear(){
        send_raw(176,0,0);
        send_raw(176,0,1);
        return;
    }


    public static void connect_by_name(string port_name){
        int success;

        0 => success;
        for(0=>int i; i<10; i++){
            launchpad_out.open(i) => success;
            if(launchpad_out.name() == port_name){
                1 => success;
                //<<<"out:",i>>>;
                break;
            }
        }
        if(!success){
            <<<"no outport named:" + port_name>>>;
        }

        0 => success;
        for(0=>int i; i<10; i++){
            launchpad_in.open(i) => success;
            if(launchpad_in.name() == port_name){
                1 => success;
                break;
            }
        }
        if(!success){
            <<<"no inport named:" + port_name>>>;
        }
    }


    public static void set(int row, int col, int color){
        if(color_is_valid(color) && pos_is_valid(row,col)){
            send(row, col, color);
        }
    }


    
    public static void send_raw(int data1, int data2, int data3){
        0 => int success;
        if(data3 == 0 || data3 == 1){
            1 => success;
        }
        for(0=>int i; i<colors.cap(); i++){
            if(data3 == colors[i]){
                1 => success;
            }
            
        }
        if(!success){
            <<<"OOPS: dont send this color ",data3," to launchpad">>>;
            return;
        }
	    data1  => midimessage.data1;
	    data2  => midimessage.data2;
	    data3  => midimessage.data3;
        if(debug && 0){
            <<<"Sending midi out...">>>;
            <<<"data1:"+ midimessage.data1>>>;
            <<<"data2:"+ midimessage.data2>>>;
            <<<"data3:"+ midimessage.data3>>>;
        }
	    launchpad_out.send(midimessage);
        0::ms => now;
    }


    public static void send(int row, int col, int color){
        if(row == -1){
            104 + col => int button;
            //<<<"button:",button>>>;
            //<<<"color:",color>>>;
            //<<<"send:",colors[color]>>>;
            send_raw(176,button,colors[color]);
            
        }
        else if(row_is_valid(row)){
            row * 16 + col => int button;
            //<<<"button:",button>>>;
            //<<<"color:",color>>>;
            //<<<"send:",colors[color]>>>;
            send_raw(144,button,colors[color]);
        }
        
    }

    public static int pos_is_valid(int row, int col){
        return(row_is_valid(row) && col_is_valid(col));
    }

    public static int row_is_valid(int row){
        return(true);
        //row >= 0 && row <= files.cap());
    }
    
    public static int col_is_valid(int col){
        return (col >= 0 && col < 8);
    }

    public static int color_is_valid(int color){
        return(color >= 0 && color < colors.cap());
    }



    public static void osc_listen(){
        int color_id;
        osc_in.event("/launchpad, i, i, s") @=> OscEvent @ oe_in;
        while(true){
            oe_in => now;
            while(oe_in.nextMsg()) {
                oe_in.getInt() => int row;
                oe_in.getInt() => int col;
                oe_in.getString() => string color;
                //oe_in.getInt() => int color;
                //<<<"row:" + row + ", col:" + col + ", color:" + color>>>;
                /*
                0 = off
                1 = light red
                2 = medium red
                3 = bright red
                4 = green
                5 = dark yellow
                6 = medium yellow
                7 = bright yellow
                */
                if(color=="clearall"){
                    clear();
                }
                else {
                    if(color=="off") 0 => color_id;
                    else if(color=="red" || color=="red3") 3 => color_id;
                    else if(color=="red1") 1 => color_id;
                    else if(color=="ged2") 2 => color_id;
                    else if(color=="green") 4 => color_id;
                    else if(color=="yellow" || color=="yellow3") 7 => color_id;
                    else if(color=="yellow1") 5 => color_id;
                    else if(color=="yellow2") 6 => color_id;
                    send(row,col,color_id);
                }

                
            }
        }
    }


    
    
    public static void midi_listen(){
        //<<<"listening">>>;
        //MidiIn midi_in;
        MidiMsg midi_msg;
        
        int event_channel, event_type;
        int pressed, row, col;        
        
        while(true){
            launchpad_in => now;
            while(launchpad_in.recv(midi_msg)){
                midi_msg.data3 / 127 => pressed;
                midi_msg.data2 % 16 => col;
                midi_msg.data2 / 16 => row;

                if(debug && false){
                    <<<"----------">>>;
                    <<<midi_msg.data1>>>;
                    <<<midi_msg.data2>>>;
                    <<<midi_msg.data3>>>;
                    <<<"channel:">>>;
                    <<<midi_msg.data1 & 0x0f>>>;
                    <<<"type:">>>;
                    <<<((midi_msg.data1 & 0xf0) >> 4)>>>;
                }

                //<<<"data1:" + midi_msg.data1 + ", pressed:" + pressed + ", row:" + row + ", col:" + col>>>;
                if(midi_msg.data1 == 176){
                    osc(-1, col-8, pressed);
                } else
                if(midi_msg.data1 == 144){
                    osc(row, col, pressed);
                    
                }

                
            }
            
        }
    }
    
}

Launchpad dummy;
1::week => now;