--[[============================================================================
main.lua
============================================================================]]--

local debug = true
_AUTO_RELOAD_DEBUG = function()
  
end

-- Read from the manifest.xml file.
class "RenoiseScriptingTool" (renoise.Document.DocumentNode)
  function RenoiseScriptingTool:__init()    
    renoise.Document.DocumentNode.__init(self) 
    self:add_property("Name", "Untitled Tool")
    self:add_property("Id", "Unknown Id")
  end

local manifest = RenoiseScriptingTool()
local ok,err = manifest:load_from("manifest.xml")
local tool_name = manifest:property("Name").value
local tool_id = manifest:property("Id").value


local OscMessage = renoise.Osc.Message
local OscBundle = renoise.Osc.Bundle

local client, socket_error = renoise.Socket.create_client(
  "localhost", 8008, renoise.Socket.PROTOCOL_UDP)
   
if (socket_error) then 
  renoise.app():show_warning(("Failed to start the " .. 
    "OSC client. Error: '%s'"):format(socket_error))
  return
end



--------------------------------------------------------------------------------
-- copied from GlobalOscActions.lua
--------------------------------------------------------------------------------
local global_action_pattern_map = table.create{}


local function argument(name, type)
  return { name = name, type = type }
end


local server, socket_error = renoise.Socket.create_server(
  "localhost", 8007, renoise.Socket.PROTOCOL_UDP)
   
if (socket_error) then 
  renoise.app():show_warning(("Failed to start the " .. 
    "OSC server. Error: '%s'"):format(socket_error))
  return
end

server:run {
   socket_message = function(socket, data)
      -- decode the data to Osc
      local message_or_bundle, osc_error = renoise.Osc.from_binary_data(data)
      
      if (message_or_bundle) then
         if (type(message_or_bundle) == "Message") then

	    
            if message_or_bundle.pattern == '/renoise/luck/load_song' then
               local xrns = message_or_bundle.arguments[1]['value']
	       if debug then
		  print("loading xrns:",xrns)
	       end
               renoise.app():load_song(xrns)
	       if debug then
		  print("loading done")
	       end

            elseif message_or_bundle.pattern == '/renoise/luck/get_song_path' then
	       local song_path = renoise.song().file_name	       
	       if debug then
              print("sending song_path:", song_path)
	       end
               client:send(
                  OscMessage("/fromrenoise/luck/song_path", { 
                                {tag="s", value=song_path}
                                                                        })
                          )
	       


            elseif message_or_bundle.pattern == '/renoise/luck/get_number_of_tracks' then
               local nb_tracks = renoise.song().sequencer_track_count
               if debug then
                  print("sending (over OSC) number of tracks:",nb_tracks)
               end
               client:send(
                  OscMessage("/fromrenoise/luck/number_of_tracks", { 
                                {tag="i", value=nb_tracks}
                                                                   })
                          )


            elseif message_or_bundle.pattern == '/renoise/luck/get_bpm' then
               local bpm = renoise.song().transport.bpm
               if debug then
                  print("sending (over OSC) bpm:",bpm)
               end
               client:send(
                  OscMessage("/fromrenoise/luck/bpm", {
                                {tag="f", value=bpm}
                                                      })
                          )


            elseif message_or_bundle.pattern == '/renoise/luck/get_track_info' then
               local track_number = message_or_bundle.arguments[1]['value']
               local track_name = trim(renoise.song():track(track_number).name)

               if track_number < 1 or track_number > renoise.song().sequencer_track_count then
                  print("invalid track number requested:",track_number)
               else
                  if debug then
                     print("sending (over OSC) track name for track",track_number,track_name)
                  end
                  client:send(
                     OscMessage("/fromrenoise/luck/track_info", { 
                                   {tag="i", value=track_number},
                                   {tag="s", value=track_name} 
                                })
                  )
               end


            elseif message_or_bundle.pattern == '/renoise/luck/get_number_of_instruments' then
               local nb_instruments = #renoise.song().instruments
               if debug then
                  print("sending (over OSC) number of instruments",nb_instruments)
               end
               client:send(
                  OscMessage("/fromrenoise/luck/number_of_instruments", { 
                                {tag="i", value=nb_instruments}
                                                                        })
                          )



            elseif message_or_bundle.pattern == '/renoise/luck/get_instrument_info' then
               local instrument_number = message_or_bundle.arguments[1]['value']
               local instrument_name = trim(renoise.song():instrument(instrument_number).name)
               if debug then
                  print("sending (over OSC) instrument name",instrument_number,instrument_name)
               end
               client:send(
                  OscMessage("/fromrenoise/luck/instrument_info", { 
                                {tag="i", value=instrument_number},
                                {tag="s", value=instrument_name} 
                                                                  })
                          )
               


            else
               print('unhandled OSC message:')
               print(message_or_bundle.pattern)
               rprint(message_or_bundle.arguments)
            end

         elseif (type(message_or_bundle) == "Bundle") then
            print(("Got OSC bundle: '%s'"):format(tostring(message_or_bundle)))
            
         else
            -- wil never get here
         end
         
      else
         print("Got invalid OSC data")
      end
      

   end    
}

-------------------------------------------------------------------------------------------
-- helpers
-------------------------------------------------------------------------------------------

function trim(s)
  return (s:gsub("^%s*(.-)%s*$", "%1"))
end


--------------------------------------------------------------------------------

function sleep(seconds)
   os.execute("sleep " .. tonumber(seconds))
end

local function notify_over_osc()
   if debug then
      print("sending (over OSC) loaded ",renoise.song().file_name)
   end


   client:send(
      OscMessage("/fromrenoise/luck/song_loaded", {
                    {tag="s", value=renoise.song().file_name}
                                                  })
              )
end




renoise.tool().app_new_document_observable:add_notifier(function()
 							   notify_over_osc()
end)

-- for some reason the first message doesn't seem to get send
-- so this is a first message that's not used by anyone
client:send(
   OscMessage("/hack", {
                 {tag="s", value="hack"}
                                               })
           )
