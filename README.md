Luck is a Live environment Using ChucK. It provides the tools to perform
electronic music live.

Basically renoise is playing back samples, triggered over OSC from ChucK.
Various libraries in ChucK are responsible for loading an xrns (song) in
renoise, keeping track of time and allowing .ck files to be added/removed
from a novation launchpad.
