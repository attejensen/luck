// launch with r.ck

// name
"localhost" => string hostname;
8010 => int port;

// check command line

// send object
OscSend xmit;

// aim the transmitter
xmit.setHost( hostname, port );

["red","red1","red2","red3","green","yellow","yellow1","yellow2","yellow3","off","clearall"] @=> string colors[];

// infinite time loop
while( true )
{
    //<<<"sending...">>>;
    xmit.startMsg( "/launchpad, i, i, s" );
    Std.rand2(0,8) => int col;
    Std.rand2(-1,7) => int row;
    colors[Std.rand2(0,colors.cap()-1)] => string color;
    //Std.rand2(0,9) => int color;

    row => xmit.addInt;
    col => xmit.addInt;
    color => xmit.addString;
    //color => xmit.addInt;
    //<<<"row:" + row + ", col:" + col + ", color:" + color>>>;
    20::ms => now;
}
