// reception
OscRecv recv;
8009 => recv.port;
recv.listen();
recv.event("/acc, f, f, f") @=> OscEvent oe;


// sending
"localhost" => string hostname;
8000 => int renoisePort;
OscSend xmit;
xmit.setHost(hostname, renoisePort);


//senders
fun void setDSP(int track, int device, int parameter, float value){
    // track: track nb in renoise 1-...
    // device: DSP device on track, 1 = input
    // parameter: DSP paramter, 1-...
    // value: value: 0-1
    xmit.startMsg("/renoise/song/track/" + track + "/device/" + device + "/set_parameter_by_index", "f, f" );
    parameter => xmit.addFloat;
    value => xmit.addFloat;
}

// mappings
fun void sendAcc1(float acc){
}

fun void sendAcc2(float acc){
    xmit.startMsg("/renoise/song/track/4/device/3/set_parameter_by_index", "f, f" );
    2 => xmit.addFloat;
    acc => xmit.addFloat;
}

// waiters
fun void waitAcc(){
    [0., 0., 0.] @=> float acc[];
    while(true) {
        <<<"before wait">>>;
        oe => now;
        <<<"after wait">>>;
        while (oe.nextMsg() != 0) {
            <<<"in while">>>;
            oe.getFloat() * .05 + .5 => acc[0];
            oe.getFloat() * .05 + .5 => acc[1];
            oe.getFloat() * .05 + .5 => acc[2];
        }

        setDSP(3,3,2,acc[0]);
        setDSP(4,3,2,acc[1]);
        setDSP(6,2,2,acc[2]);

        <<<"acc:", acc[0], acc[1], acc[2]>>>; 
    }
}

spork ~ waitAcc();

1::week => now;