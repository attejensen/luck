
new MidiOut @=> MidiOut @ launchpad_out;
new MidiIn @=> MidiIn @ launchpad_in;

connect_by_name("Launchpad MIDI 1");// => out_device;

listen();



fun void connect_by_name(string port_name){
    int success;
    
    0 => success;
    for(0=>int i; i<10; i++){
        launchpad_out.open(i) => success;
        if(launchpad_out.name() == port_name){
            1 => success;
            <<<"out:",i>>>;
            break;
        }
    }
    if(!success){
        <<<"no outport named:" + port_name>>>;
    }
    
    0 => success;
    for(0=>int i; i<10; i++){
        launchpad_in.open(i) => success;
        if(launchpad_in.name() == port_name){
            1 => success;
            break;
        }
    }
    if(!success){
        <<<"no outport named:" + port_name>>>;
    }
}


fun void listen(){
    //<<<"listening">>>;
    //MidiIn midi_in;
    MidiMsg midi_msg;
    
    int event_channel, event_type;
    int pressed, row, col;        
    
    while(true){
        launchpad_in => now;
        while(launchpad_in.recv(midi_msg)){
            midi_msg.data3 / 127 => pressed;
            <<<"----------">>>;
            <<<midi_msg.data1>>>;
            <<<midi_msg.data2>>>;
            <<<midi_msg.data3>>>;
            <<<"channel:">>>;
            <<<midi_msg.data1 & 0x0f>>>;
            <<<"type:">>>;
            <<<((midi_msg.data1 & 0xf0) >> 4)>>>;
        }
    }
}
