// reception
OscRecv recv;
8009 => recv.port;
recv.listen();
recv.event("/acc, f, f, f") @=> OscEvent oe;


// sending
"localhost" => string hostname;
8000 => int renoisePort;
OscSend xmit;
xmit.setHost(hostname, renoisePort);



fun void start(){
    xmit.startMsg("/renoise/transport/start,");
}

fun void stop(){
    xmit.startMsg("/renoise/transport/stop,");
}

fun void set_bpm(float bpm){
    xmit.startMsg("/renoise/song/bpm","f");
    bpm => xmit.addFloat;
}

fun void note_on(int instrument, int track, int note, int velocity){
    xmit.startMsg("/renoise/trigger/note_on, i, i, i, i");
    instrument => xmit.addInt;
    track => xmit.addInt;
    note => xmit.addInt;
    velocity => xmit.addInt;
}

fun void test(){
    xmit.startMsg("/renoise/song/track/01/getname,");
}


//1::second => now;

/*    
xmit.startMsg("/renoise/song/track/4/device/3/set_parameter_by_index", "f, f" );
2 => xmit.addFloat;
acc => xmit.addFloat;
*/
/*
while(true)
{
    note_on(2,0,48,128);
    .125::second => now;
    //1::week => now;
}
*/



while(true){
    set_bpm(Std.rand2f(120,140));
    <<<"sending">>>;
    test();
    1::second => now;
}