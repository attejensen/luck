// receiver
OscRecv recv;
8008 => recv.port;
recv.listen();

//sender
"localhost" => string hostname;
8000 => int renoisePort;
OscSend xmit;
xmit.setHost(hostname, renoisePort);


int number_of_tracks;
64 => int max_number_of_tracks;
string track_names[max_number_of_tracks];

int number_of_instruments;
64 => int max_number_of_instruments;
string instrument_names[max_number_of_instruments];


load_song("/home/atte/music/renoise/projects/live/fodspor.xrns");

setup_tracks();
setup_instruments();

for(0=>int i; i<number_of_instruments; i++){
    <<<instrument_names[i]>>>;
}

fun void setup_tracks(){
    get_number_of_tracks() => number_of_tracks;
    for(1 => int i; i <= number_of_tracks; i++){
        get_track_name(i) => track_names[i-1];
    }
}

fun void setup_instruments(){
    get_number_of_instruments() => number_of_instruments;
    for(1 => int i; i <= number_of_instruments; i++){
        get_instrument_name(i) => instrument_names[i-1];
    }
}




fun int get_number_of_tracks(){
    xmit.startMsg("/renoise/song/get_number_of_tracks,");
    recv.event("/fromrenoise/song/number_of_tracks, i") @=> OscEvent @ oe_in;

    oe_in => now;
    while( oe_in.nextMsg() )
    {
        return oe_in.getInt();
    }
}

fun string get_track_name(int track_number){
    string got_string;
    int got_int;

    xmit.startMsg("/renoise/song/get_track_info, i");
    track_number => xmit.addInt;
    recv.event("/fromrenoise/song/track_info, i, s") @=> OscEvent @ oe_in;

    oe_in => now;
    while( oe_in.nextMsg() )
    {
        oe_in.getInt() => got_int;
        oe_in.getString() => got_string;
        if(got_int == track_number){
            //<<<"track",got_string>>>;
            return got_string;
        }
    }
}



fun int get_number_of_instruments(){
    xmit.startMsg("/renoise/song/get_number_of_instruments,");
    recv.event("/fromrenoise/song/number_of_instruments, i") @=> OscEvent @ oe_in;

    oe_in => now;
    while( oe_in.nextMsg() )
    {
        return oe_in.getInt();
    }
}

fun string get_instrument_name(int instrument_number){
    string got_string;
    int got_int;

    xmit.startMsg("/renoise/song/get_instrument_info, i");
    instrument_number => xmit.addInt;
    recv.event("/fromrenoise/song/instrument_info, i, s") @=> OscEvent @ oe_in;

    oe_in => now;
    while( oe_in.nextMsg() )
    {
        oe_in.getInt() => got_int;
        oe_in.getString() => got_string;
        if(got_int == instrument_number){
            return got_string;
        }
    }
}


fun float get_bpm(){
    //<<<"in get bpm">>>;
    xmit.startMsg("/renoise/song/get_bpm,");
    recv.event("/fromrenoise/song/bpm, f") @=> OscEvent @ oe_in;

    oe_in => now;
    while( oe_in.nextMsg() )
    {
        return oe_in.getFloat();
    }
}

fun void load_song(string xrns){
    string loaded_song;
    xmit.startMsg("/renoise/load_song, s");
    xrns => xmit.addString;
    recv.event("/fromrenoise/song_loaded, s") @=> OscEvent @ oe_in;

    oe_in => now;
    while( oe_in.nextMsg() )
    {
        oe_in.getString() => loaded_song;
        if(loaded_song == xrns){
            return;
        }
        else {
            <<<"Error, song ",xrns," not loaded">>>;
        }
    }
}
