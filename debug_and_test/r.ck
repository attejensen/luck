// create our OSC receiver
OscRecv recv;
// use port 6449 (or whatever)
8008 => recv.port;
// start listening (launch thread)
recv.listen();

// create an address in the receiver, store in new variable
recv.event( "/luck/transport/bpm, f" ) @=> OscEvent @ oe;

// infinite event loop
while( true )
{
    // wait for event to arrive
    oe => now;

    // grab the next message from the queue. 
    while( oe.nextMsg() )
    {
        <<<oe.getString()>>>;


        // print
        //<<< "got (via OSC):", i, f >>>;
    }
}
