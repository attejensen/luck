#!/usr/bin/python
# -*- coding: utf-8 -*-
import liblo, os, sys

port = 8011

if len(sys.argv) > 1:
    port = sys.argv[1]


def debug(path, args, type):
    print('port:' + str(port) + ' ' + str(path) + ' ' + str(args))

def start_server(port):
    try:
        return liblo.Server(port)
    except liblo.ServerError, err:
        print str(err)
        sys.exit()

server = start_server(port)
server.add_method(None, None, debug)

while True:
    server.recv(10)

