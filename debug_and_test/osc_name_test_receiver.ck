OscRecv recv;
8008 => recv.port;
recv.listen();

recv.event( "/fromrenoise/song/track_info, i, s" ) @=> OscEvent @ oe;
//recv.event( "/fromrenoise/song/number_of_track, i" ) @=> OscEvent @ oe;

int track_number, track_type, number_of_tracks;
string track_name;

// infinite event loop
while( true )
{
    //<<<"waiting">>>;
    // wait for event to arrive
    oe => now;
    //<<<"got message">>>;
    // grab the next message from the queue. 
    while( oe.nextMsg() )
    {
        oe.getInt() => track_number;
        oe.getString() => track_name;
        //<<<"doing something with message">>>;
        //<<<"Track:",oe.getInt(),"Name:",oe.getString()>>>;
        <<<"Track:",track_number,"Name:",track_name>>>;


        // print
        //<<< "got (via OSC):", i, f >>>;
    }
}
