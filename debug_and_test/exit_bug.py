#!/usr/bin/python
import pygame.midi
pygame.init()
pygame.midi.init()
pygame.fastevent.init()
for i in range( pygame.midi.get_count()):
    (interf, name, input, output, opened) = pygame.midi.get_device_info(i)
    if 'Launchpad' in name and input:
        from_launchpad = pygame.midi.Input(i, 0)
    if 'Launchpad' in name and output:
        to_launchpad = pygame.midi.Output(i, 0)
del from_launchpad
del to_launchpad
pygame.midi.quit()
