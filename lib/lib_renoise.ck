public class Renoise {
    0 => static int debug;
    0 => static int bypass;
    
    // reception
    new OscRecv @=> static OscRecv @ from_renoise_luck;
    8008 => from_renoise_luck.port;
    from_renoise_luck.listen();
    
    // sending
    "localhost" => string hostname;
    8000 => int renoise_port;
    8007 => int luck_port;

    new OscSend @=> static OscSend @ to_renoise;
    to_renoise.setHost(hostname, renoise_port);
    new OscSend @=> static OscSend @ to_renoise_luck;
    to_renoise_luck.setHost(hostname, luck_port);

    10::ms => static dur osc_wait;
    
    static int number_of_tracks;
    64 => static int max_number_of_tracks;
    new string[max_number_of_tracks] @=> static string track_names[];

    
    static int number_of_instruments;
    64 => static int max_number_of_instruments;
    new string[max_number_of_instruments] @=> static string instrument_names[];

    

    0 => static int loaded;
    0 => static int ready;

    if(!bypass){
        if(debug)<<<"not bypassing renoise">>>;
        if(debug)<<<"a">>>;
        wait_for_load();
        if(debug)<<<"b">>>;
        get_bpm() => Time.tempo;
        //spork ~ osc_bpm_listen();
        if(debug)<<<"c">>>;
        setup_tracks();
        if(debug)<<<"d">>>;
        setup_instruments();
        if(debug)<<<"e">>>;
        setup_dsp();
        if(debug)<<<"f">>>;
        1 => ready;
        1 => Time.start_time_stick;
    }
    else {
        if(debug)<<<"bypassing renoise">>>;
    }
    10::ms => now;
    Time.start();


    
    // -----------------------------------------------------------------------

    public static void setup_dsp(){
        <<<"just do it...!">>>;
    }
    
    public static void setup_tracks(){
        get_number_of_tracks() => number_of_tracks;
        for(1 => int i; i <= number_of_tracks; i++){
            get_track_name(i) => track_names[i-1];
            <<<"just got track name " + i + ":" + track_names[i-1]>>>;
            //10::ms => now;
        }
    }
    
    public static void setup_instruments(){
        get_number_of_instruments() => number_of_instruments;
        for(1 => int i; i <= number_of_instruments; i++){
            get_instrument_name(i) => instrument_names[i-1];
            //10::ms => now;
        }
    }


    public static int get_number_of_tracks(){
        if(debug){<<<"in get_number_of_tracks">>>;}

        from_renoise_luck.event("/fromrenoise/luck/number_of_tracks, i") @=> OscEvent @ oe_in;
        spork ~ osc_request("/renoise/luck/get_number_of_tracks");
        

        
        oe_in => now;
        while( oe_in.nextMsg() )
        {
            oe_in.getInt() => int number_of_tracks;
            if(debug)<<<"got nb tracks:",number_of_tracks>>>;
            return number_of_tracks;
        }
        
    }


    // -----------------------

    
    public static void osc_request(string address){
        osc_wait => now;
        //address + "," => address;
        to_renoise_luck.startMsg(address + ",");
        
    }

    public static void osc_request(string address, int arg1){
        osc_wait => now;
        //address + ", i" => address;
        to_renoise_luck.startMsg(address,"i");
        arg1 => to_renoise_luck.addInt;
    }

    public static void osc_request(string address, string arg1){
        osc_wait => now;
        //address + ", s" => address;
        to_renoise_luck.startMsg(address,"s");
        arg1 => to_renoise_luck.addString;
    }

    
    // --------------------------

    
    public static string get_track_name(int track_number){
        if(debug){<<<"in get_track_name, track_number:",track_number>>>;}
        string track_name;
        int got_track_number;
        
        from_renoise_luck.event("/fromrenoise/luck/track_info, i, s") @=> OscEvent @ oe_in;
        spork ~ osc_request("/renoise/luck/get_track_info", track_number);

        
        oe_in => now;
        while( oe_in.nextMsg() )
        {
            oe_in.getInt() => got_track_number;
            oe_in.getString() => track_name;
            if(got_track_number == track_number){
                if(debug)<<<"got name",track_name,"for track number",got_track_number>>>;
                return track_name;
            }
            else {
                if(debug){<<<"OOPS, got track number",got_track_number,"but expected",track_number>>>;}
            }
        }
    }
    


    public static int get_number_of_instruments(){
        //to_renoise_luck.startMsg("/renoise/luck/get_number_of_instruments,");
        
        from_renoise_luck.event("/fromrenoise/luck/number_of_instruments, i") @=> OscEvent @ oe_in;
        spork ~ osc_request("/renoise/luck/get_number_of_instruments");
        
        oe_in => now;
        while( oe_in.nextMsg() )
        {
            oe_in.getInt() => int number_of_instruments;
            if(debug)<<<"got nb instr:", number_of_instruments>>>;
            return number_of_instruments;
        }
    }

    public static string instrument_name(int instrument_number){
        if(instrument_number < 0 || instrument_number >= max_number_of_instruments){
            return "";
        }
        else {
            return instrument_names[instrument_number];
        }
    }

    public static string track_name(int track_number){
        if(track_number < 0 || track_number >= max_number_of_tracks){
            return "";
        }
        else {
            return track_names[track_number];
        }
    }

    public static string get_instrument_name(int instrument_number){
        if(debug){<<<"in get_instrument_name, instrument_number:",instrument_number>>>;}
        string got_instrument_name;
        int got_instrument_number;
        
        //to_renoise_luck.startMsg("/renoise/luck/get_instrument_info, i");
        //instrument_number => to_renoise_luck.addInt;
        spork ~ osc_request("/renoise/luck/get_instrument_info", instrument_number);
        
        from_renoise_luck.event("/fromrenoise/luck/instrument_info, i, s") @=> OscEvent @ oe_in;
        
        oe_in => now;
        while( oe_in.nextMsg() )
        {
            oe_in.getInt() => got_instrument_number;
            oe_in.getString() => got_instrument_name;
            if(got_instrument_number == instrument_number){
                if(debug){<<<"got name '",got_instrument_number,"'for instrument number",got_instrument_name>>>;}
                return got_instrument_name;
            }
            else {
                if(debug){<<<"OOPS, got instrument number",got_instrument_number,"but expected",instrument_number>>>;}
            }
        }
    }
    

    public static float get_bpm(){
        if(debug){<<<"in get_bpm">>>;}
        //to_renoise_luck.startMsg("/renoise/luck/get_bpm,");
        spork ~ osc_request("/renoise/luck/get_bpm");
        
        from_renoise_luck.event("/fromrenoise/luck/bpm, f") @=> OscEvent @ oe_in;
        <<<"before wait for bpm">>>;
        oe_in => now;
        <<<"after wait">>>;
        while(oe_in.nextMsg()) {
            <<<"in while">>>;
            oe_in.getFloat() => float bpm;
            if(debug)<<<"got bpm:",bpm>>>;
            return bpm;
        }
    }
    
    
    public static void osc_bpm_listen(){
        if(debug){<<<"in get_bpm">>>;}
        //to_renoise_luck.startMsg("/renoise/luck/get_bpm,");
        //spork ~ osc_request("/renoise/luck/get_bpm");
        
        from_renoise_luck.event("/fromrenoise/luck/bpm, f") @=> OscEvent @ oe_in;
        <<<"before wait">>>;
        oe_in => now;
        <<<"after wait">>>;
        while(oe_in.nextMsg()) {
            <<<"in while">>>;
            oe_in.getFloat() => float bpm;
            if(debug)<<<"got bpm:",bpm>>>;
            bpm => Time.tempo;
        }
    }
    public static void bypass_load(){
        // use only when building songs...
        0 => Time.start_time_stick;
        1 => loaded;
    }
    
    public static void load(string xrns){
        if(loaded){
            return;
        }
        0 => Time.start_time_stick;
        //to_renoise_luck.startMsg("/renoise/luck/load_song, s");
        //xrns => to_renoise_luck.addString;
        from_renoise_luck.event("/fromrenoise/luck/song_loaded, s") @=> OscEvent @ oe_in;
        spork ~ osc_request("/renoise/luck/load_song", xrns);
        
        oe_in => now;
        while( oe_in.nextMsg() )
        {
            oe_in.getString() => string loaded_song;
            <<<"loaded song:", loaded_song>>>;
            if(loaded_song == xrns){
                1 => loaded;
                if(debug)<<<"ok, got loaded back">>>;
                //1 => Time.start_time_stick;
                return;
            }
            else {
                <<<"Error, song ",xrns," not loaded">>>;
            }
        }
        
    }
    

    public static void wait_for_load(){
        <<<"waiting for renoise to load song...">>>;
        while(!loaded){
            10::ms => now;
        }
        <<<"song loaded...">>>;
    }

    public static void wait_for_ready(){
        <<<"waiting for setup over OSC...">>>;
        while(!ready){
            10::ms => now;
        }
        <<<"setup done...">>>;
    }

    // -----------------------------------------------------------------------
    
    
    public static void start(){
        to_renoise.startMsg("/renoise/transport/start,");
    }
    
    public static void stop(){
        to_renoise.startMsg("/renoise/transport/stop,");
    }
    
    public static void set_bpm(float bpm){
        to_renoise.startMsg("/renoise/song/bpm","f");
        bpm => to_renoise.addFloat;
    }

    public static int track_name2id(string name){
        for(0=>int i; i<number_of_tracks; i++){
            if(track_names[i] == name){
                return i;
            }
        }
        return -1;
    }
    
    public static int instrument_name2id(string name){
        for(0=>int i; i<number_of_instruments; i++){
            if(instrument_names[i] == name){
                return i;
            }
        }
        return -1;
    }


    public static int str2midi(string note){
        4 => int octave;
        -1 => int root;

        if(note.find("c") == 0 || note.find("C") == 0) 0 => root;
        else if(note.find("d") == 0 || note.find("D") == 0) 2 => root;
        else if(note.find("e") == 0 || note.find("E") == 0) 4 => root;
        else if(note.find("f") == 0 || note.find("F") == 0) 5 => root;
        else if(note.find("g") == 0 || note.find("G") == 0) 7 => root;
        else if(note.find("a") == 0 || note.find("A") == 0) 9 => root;
        else if(note.find("b") == 0 || note.find("B") == 0) 11 => root;
        else if(note.find("h") == 0 || note.find("H") == 0) 11 => root;

        if(root == -1) {
            return -1;
        }

        if(note.find("0") >= 0) 0 => octave;
        else if(note.find("1") >= 0) 1 => octave;
        else if(note.find("2") >= 0) 2 => octave;
        else if(note.find("3") >= 0) 3 => octave;
        else if(note.find("4") >= 0) 4 => octave;
        else if(note.find("5") >= 0) 5 => octave;
        else if(note.find("6") >= 0) 6 => octave;
        else if(note.find("7") >= 0) 7 => octave;
        else if(note.find("8") >= 0) 8 => octave;

        if(note.find("#") >= 0) root++;
        if(note.rfind("b") >= 1) root--;

        return root + octave * 12;

    }
    

    

    public static void note_on(string instrument, string track, int note, dur duration){
        note_on(instrument, track, note, 128, duration);
    }
    
    public static void note_on(string instrument, string track, int note, int velocity, dur duration){
        note_on(instrument, track, note, velocity);
        duration => now;
        note_off(instrument, track, note);
    }


    public static void note_on(string instrument, string track){
        note_on(instrument, track, 48);
    }

    public static void note_on(string instrument, string track, int notes[]){
        for(0=>int i; i<notes.cap(); i++){
            note_on(instrument,track,notes[i]);
        }
    }
        
    public static void note_on(string instrument, string track, string notes[]){
        for(0=>int i; i<notes.cap(); i++){
            //note_on(instrument,track,str2midi(notes[i]));
            note_on(instrument,track,notes[i]);
        }
    }
        
    public static void note_on(string instrument, string track, int note){
        note_on(instrument, track, note, 128);
    }

    public static void note_on(string instrument, string track, string note){
        note_on(instrument, track, note, 128);
    }

    public static void note_on(string instrument, string track, int note, int velocity){
        _note_on(instrument_name2id(instrument), track_name2id(track), note, velocity);
    }
    
    public static void note_on(string instrument, string track, string note, int velocity){
        _note_on(instrument_name2id(instrument), track_name2id(track), str2midi(note), velocity);
    }
    
    
    public static void _note_on(int instrument, int track, int note, int velocity){
        //if(debug)<<<"note on, instr:",instrument_name(instrument),"track",track,"velocity",velocity>>>;
        if(note < 0 || note > 128){
            return;
        }

        //if(note < 0) 0 => note;
        //if(note > 128) 128 => note;

        if(velocity < 0) 0 => velocity;
        if(velocity > 128) 128 => velocity;

        if(debug)<<<"note on, instr:",instrument_name(instrument),"track",track_name(track),"velocity",velocity>>>;

        to_renoise.startMsg("/renoise/trigger/note_on, i, i, i, i");
        instrument => to_renoise.addInt;
        track => to_renoise.addInt;
        note => to_renoise.addInt;
        velocity $ int => to_renoise.addInt;
    }



    
    public static void note_off(string instrument, string track, int notes[]){
        for(0=>int i; i<notes.cap();i++){
            note_off(instrument, track, notes[i]);
        }
    }


    public static void note_off(string instrument, string track, string notes[]){
        for(0=>int i; i<notes.cap();i++){
            //<<<"string note off:",notes[i]>>>;
            note_off(instrument, track, notes[i]);
        }
    }

    
    public static void note_off(string instrument, string track){
        for(0=>int i; i<=128; i++){
            note_off(instrument, track, i);
        }
    }
    
    public static void note_off(string instrument, string track, int note){
        _note_off(instrument_name2id(instrument), track_name2id(track), note);
    }

    public static void note_off(string instrument, string track, string note){
        _note_off(instrument_name2id(instrument), track_name2id(track), str2midi(note));
    }


    
    public static void _note_off(int instrument, int track, int note){
        if(note < 0 || note > 128){
            return;
        }
        //<<<"sending note off:" + note>>>;
        to_renoise.startMsg("/renoise/trigger/note_off, i, i, i");
        instrument => to_renoise.addInt;
        track => to_renoise.addInt;
        note => to_renoise.addInt;
        //velocity => to_renoise.addInt;
    }


    public static void mute(string track[]){
        for(0=>int i; i<track.cap(); i++){
            mute(track[i]);
        }
    }
    
    public static void unmute(string track[]){
        for(0=>int i; i<track.cap(); i++){
            unmute(track[i]);
        }
    }
    
    public static void mute(string track){
        track_name2id(track)+1 => int track_id;
        to_renoise.startMsg("/renoise/song/track/"+track_id+"/mute");
    }

    public static void unmute(string track){
        track_name2id(track)+1 => int track_id;
        to_renoise.startMsg("/renoise/song/track/"+track_id+"/unmute");
    }



}

Renoise dummy;
1::week => now;