public class Global {
    0 => static int transpose;
    
    0 => static int concert;
    0 => static int debug;
    1 => static int click;
    1 => static int useBass;
    1 => static int useTransitions;

    0 => static int useNewParts;
    
    static int root;
    static int bass;
    static int chrom[];
    static int dia[];
    static int penta[];
    static int triad[];
    static int chord[];

    static int mel[];
    static int lengths[];
    
    //static int chunks;

    static int part;
    
    90 => static float filterRange;
    30 => static float filterLow;

    [.5, .5, .5, .5, .5, .5, .5, .5] @=> static float upper[];
    [.5, .5, .5, .5, .5, .5, .5, .5] @=> static float middle[];
    [.5, .5, .5, .5, .5, .5, .5, .5] @=> static float lower[];

    //debug => MyMidiOut.debug;

    if(concert){
        0 => debug;
        1 => click;
        0 => useBass;
        1 => useTransitions;
    }

    public static void setHeight(float value){
        if(debug)<<<"inside globals->setHeight">>>; 
        //Csound.setMeta(2,value);
        value => lower[2];
    }
    public static float getHeight(){
        return lower[2];
    }
    public static float getHeight(float lo, float hi){
        // returns between lo and hi
        return (lower[2] * (hi - lo)) + lo;
    }


    
    public static void setDynamic(float value){
        if(debug)<<<"inside globals->setDynamic">>>; 
        //Csound.setMeta(7,value);
        value => lower[7];
    }
    public static float getDynamic(){
        return lower[7];
    }
    public static float getDynamic(float lo, float hi){
        // returns between lo and hi
        return (lower[7] * (hi - lo)) + lo;
    }


    
    public static void setActivity(float value){
        if(debug)<<<"inside globals->setActivity">>>; 
        //Csound.setMeta(6,value);
        value => lower[6];
    }
    public static float getActivity(){
        return lower[6];
    }
    public static float getActivity(float lo, float hi){
        // returns between lo and hi
        return (lower[6] * (hi - lo)) + lo;
    }



    
    public static void setAtonality(float value){
        if(debug)<<<"inside globals->setAtonality">>>; 
        //Csound.setMeta(1,value);
        value => lower[1];
    }
    public static float getAtonality(){
        return lower[1];
    }
    public static float getAtonality(float lo, float hi){
        // returns between lo and hi
        return (lower[1] * (hi - lo)) + lo;
    }



    

    
    public static void setLength(float value){
        if(debug)<<<"inside globals->setLength">>>; 
        //Csound.setMeta(5,value);
        value => lower[5];
    }
    public static float getLength(){
        return lower[5];
    }
    public static float getLength(float lo, float hi){
        // returns between lo and hi
        return (lower[5] * (hi - lo)) + lo;
    }



    
    public static void setFilter1(float value){
        if(debug)<<<"inside globals->setFilter1">>>; 
        //Csound.setMeta(3,value);
        value => lower[3];
    }
    public static float getFilter1(){
        return lower[3];
    }
    public static float getFilter1(float lo, float hi){
        // returns between lo and hi
        return (lower[3] * (hi - lo)) + lo;
    }



    
    public static void setFilter2(float value){
        if(debug)<<<"inside globals->setFilter2">>>; 
        //Csound.setMeta(4,value);
        value => lower[4];
    }
    public static float getFilter2(){
        return lower[4];
    }
    public static float getFilter2(float lo, float hi){
        // returns between lo and hi
        return (lower[4] * (hi - lo)) + lo;
    }




    public static float quantize(float input, int scale[]){
        128 => float best_distance;
        float this_distance;
        0 => float best;
        int try;
        for(0=>int i; i<11; i++){
            for(0=>int j; j<scale.cap(); j++){
                scale[j]+i*12 => try;
                Std.fabs(try - input) => this_distance;
                if(this_distance < best_distance){
                    this_distance => best_distance;
                    try => best;
                }
            }
        }
        return best;
    }

    
    public static int nextInScale(int current, int direction, int scale[]){
        int try, octave;
        
        if(direction > 0){
            0 => try;
            while(try < current){
                for(0=>octave; octave < 10; octave++){
                    for(0=>int i; i< scale.cap(); i++){
                        scale[i] + (octave * 12) => try;
                        if(try > current){
                            return try;
                        }
                    }
                    
                }
            }
        }
        else {
            200 => try;
            while(try > current){
                for(10=>octave; octave > 0; octave--){
                    for(scale.cap()-1 => int i; i>=0; i--){
                        scale[i] + (octave * 12) => try;
                        if(try < current){
                            return try;
                        }
                    }
                    
                }
            }
        }
    }    

    // automatically tune beat lasting 1/4, 1/2, 1, 2 (etc) bars
    /*
    public static void tuneBeat(SndBuf s){
        tuneBeat(s,160);
    }

    public static void tuneBeat(SndBuf s, float maxBpm){
        float guessBpm;
        64 => float guessBars;
        
        (1::second / 1::samp * 60 / maxBpm * 4) $ int => int samplesPrBar;
        
        (s.length() / 1::samp) $ int => int nbSamples;
        while(nbSamples < samplesPrBar * guessBars){
            guessBars * .5 => guessBars;
        }
        
        1::second / s.length() * 60 * 4 * guessBars => guessBpm;
        
        Time.tempo/guessBpm => s.rate;
    }

    public static void tuneSample(SndBuf s, float root, float note, float detune){
        detune * Std.mtof(note) / Std.mtof(root) => s.rate;
    }

    public static void tuneSample(SndBuf s, float root, float note){
        tuneSample(s,root,note,1);
    }
    
    // sample player
    public static void sampler(SndBuf s, float root, float note){
        Std.mtof(note)/Std.mtof(root) => float rate;
        sampler(s,rate,Time.beat(),-1,-1,-1,-1);
    }
    
    public static void sampler(SndBuf s, float root, float note, dur length, int loop){
        Std.mtof(note)/Std.mtof(root) => float rate;
        if(loop){
            s.samples() =>  int sample_length;
            sampler(s,rate,length,0,sample_length,0,sample_length);
        }
        else {
            sampler(s,rate,length,-1,-1,-1,-1);
        }
    }
    
    public static void sampler(SndBuf s, float root, float note, dur length, int loop_start, int loop_length){
        Std.mtof(note)/Std.mtof(root) => float rate;
        sampler(s,rate,length,0,s.samples(),loop_start,loop_start);
    }
    
    
    
    public static void sampler(SndBuf s, float rate, dur length, int sample_start, int sample_end, int loop_start, int loop_end){
        0 => int loop;
        0 => int loop_length;
        now  + length => time end;
        if(loop_start >= 0 && loop_end > 0 &&loop_start < loop_end && loop_end <= s.samples()){
            loop_end - loop_start => loop_length;
            1 => loop;
        }
        rate => s.rate;
        
        if(loop){
            Math.max(0, sample_start) $ int => s.pos;
            (s.pos() + loop_start + loop_length)::samp => now;
            while(now < end){
                loop_start => s.pos;
                if(end - now > loop_length::samp){
                    loop_end::samp => now;
                }
                else {
                    end => now;
                }
            }
        }
        else {
            Math.max(0, sample_start) $ int => s.pos;
            length => now;
        }
    }    

    public static void sampleLooper(SndBuf s){
        0 => s.pos;
        1 => s.loop;
    }
    */


    /*
    public static void vocal(Pan2 out,
    float fxGain,
    float lisaGain, int lisaSub,
    float detune,
    dur delTimeLeft, dur delTimeRight, float delLeftGain, float delRightGain,
    float reverbMix){
        1 => int FXdebug;

        adc => Gain in;
        fxGain => in.gain;
        Gain postLisa;
        Pan2 postPitch;
        Pan2 postDelay;
        Pan2 postReverb;

        // lisa
        [new LiSa, new LiSa] @=> LiSa l[];


        // reverse delay
        in => postLisa;
        if(lisaGain){
            if(FXdebug)<<<"lisa ON">>>;
            for(0=>int i; i<l.cap(); i++){
                lisaGain => l[i].gain;
                in => l[i] => postLisa;
                Time.beat() * 16 * Time.get_meter() => l[i].duration;
                -1. => l[i].rate;
            }
        }
        else {
            if(FXdebug)<<<"lisa OFF">>>;
        }

        // pitch shift
        if(detune > 0){
            if(FXdebug)<<<"pitch ON">>>;
            postLisa => PitShift pl1;
            postLisa => PitShift pr1;
            postLisa => PitShift pl2;
            postLisa => PitShift pr2;
            
            1 + detune => pl1.shift;
            1 - detune => pr1.shift;
            1 - (detune * .5) => pl2.shift;
            1 + (detune * .5) => pr2.shift;
            
            1 => pl1.mix => pr1.mix;
            1 => pl2.mix => pr2.mix;
            
            pl1 => postPitch.left;
            pl2 => postPitch.left;
            pr1 => postPitch.right;
            pr2 => postPitch.right;
        }
        else {
            if(FXdebug)<<<"pitch OFF">>>;
            postLisa => postPitch;
        }

        
        // delay
        if(delLeftGain > 0 || delRightGain > 0){
            if(FXdebug)<<<"delay ON">>>;
            postPitch.left => Delay el => postDelay.left;
            postPitch.right => Delay er => postDelay.right;
            delTimeLeft => el.max => el.delay;
            delTimeRight => er.max => er.delay;
            delLeftGain => el.gain;
            delRightGain => er.gain;
        }
        else {
            if(FXdebug)<<<"delay OFF">>>;
        }
        postPitch.left => postDelay.left;
        postPitch.right => postDelay.right;


        // reverb
        if(reverbMix > 0){
            if(FXdebug)<<<"reverb ON">>>;
            postDelay.left => NRev rl => postReverb.left;
            postDelay.right => NRev rr => postReverb.right;
            reverbMix => rl.mix => rr.mix;
        }
        else {
            if(FXdebug)<<<"reverb OFF">>>;
            postDelay => postReverb;
        }

        // output
        postReverb => out;

        // lisa loop
        if(lisaGain){
            0 => int i;
            while(true){
                l[i].record(1);
                Time.wait(lisaSub);
                l[i].rampUp(10::ms);
                (i + 1) % l.cap() => i;
                l[i].rampDown(10::ms);
            }
        }

        1::week => now;
    }
    


    public static void vox(UGen in, Pan2 out){
        .15 => float filtMix;

        int silent, on;
        .05 => float threshold;
        time lastOn;
        100::ms => dur onWarn;
        10000 => int length;
        1200 => float freqHi;
        //600 => float freqLo;
        .79 => float q;

        .006 => float detune;
        
        in => BRF fHi => Pan2 listen => Pan2 ng => out;
        //freqLo => fLo.freq;
        freqHi => fHi.freq;
        //q => fLo.Q;
        q => fHi.Q;

        1 => listen.gain => ng.gain;
        
        fHi => Echo e1 => PitShift p1 => ng;
        e1 => PitShift p2 => out;
        
        fHi => Echo e2 => PitShift p3 => ng.left;
        e2 => PitShift p4 => ng.right;

        in => Gain dryMix => listen;
        filtMix => fHi.gain;
        1 - filtMix => dryMix.gain;
        

        
        1 + detune => p1.shift;
        1 - detune => p2.shift;
        1 + (detune * 2) => p3.shift;
        1 - (detune * 2) => p4.shift;
        1 => p1.mix => p3.mix;
        1 => p2.mix => p4.mix;;
        5::ms => e1.max => e1.delay;
        10::ms => e2.max => e2.delay;
        1 => e1.mix;
        1 => e2.mix;
        .2 => e1.gain;
        .4 => e2.gain;

        //1::week => now;

        while(true){
            if(listen.last() > threshold){
                //<<<"on">>>;
                1 => ng.gain;
                0 => silent;
            }
            else {
                silent++;
            }
            if(silent > length){
                //<<<"off">>>;
                0 => ng.gain;
            }
            10::samp => now;
        }
    }

    public static void voxProcOld(UGen in, UGen out, float dryGain, float delaySubDiv, float lisaGain){
        //new BRF @=> BRF f;
        //new Chorus @=> Chorus c;
        //new Gain @=> Gain g;
        BRF f;
        Chorus c;
        Gain g;
        [new LiSa, new LiSa] @=> LiSa l[];
        for(0=>int i; i<l.cap(); i++){
            //in => c => f => l[i] => out;
            //in => c => f => out;
            in => f => out;
            delaySubDiv * Time.beat() * 4 * Time.get_meter() => l[i].duration;
            -1 => l[i].rate;
            lisaGain => l[i].gain;
        }
        // chorus
        .001 => c.modFreq;
        1 => c.modDepth;
        .3 => c.mix;
        
        //f => g => out;
        dryGain => g.gain;
        
        //filter
        1200 => f.freq;
        .79 => f.Q;
        
        0 => int i;
        while(true){
            l[i].record(1);
            Time.wait(delaySubDiv);
            l[i].rampUp(10::ms);
            (i + 1) % l.cap() => i;
            l[i].rampDown(10::ms);
        }
    }
    */



    
    public static void setGain(UGen ugen, float targetGain, dur changeTime, dur slice){
        // sets the gain over a requested time
        // useful to avoid clicks
        (changeTime/slice) $ int => int nbSlices;
        (targetGain - ugen.gain()) / nbSlices => float gainChange;
        
        for(0 => int i; i<nbSlices; i++){
            ugen.gain() + gainChange => ugen.gain;
            slice => now;
        }
        // in case of rounding, set the desired target at the end
        targetGain => ugen.gain;
    }

    public static void setGain(UGen ugen, float targetGain, dur changeTime){
        setGain(ugen,targetGain,changeTime,1::ms);
    }
    
    public static void setGain(UGen ugen1, UGen ugen2, float targetGain, dur changeTime){
        setGain(ugen1,targetGain,changeTime);
        setGain(ugen2,targetGain,changeTime);
    }
    
    public static void setFilterFreq(FilterBasic filter, float targetFreq, dur changeTime, dur slice){
        // sets the filter freq over a requested time
        // useful to avoid clicks
        (changeTime/slice) $ int => int nbSlices;
        (targetFreq - filter.freq()) / nbSlices => float freqChange;
        
        for(0 => int i; i<nbSlices; i++){
            filter.freq() + freqChange => filter.freq;
            slice => now;
        }
        // in case of rounding, set the desired target at the end
        targetFreq => filter.freq;
    }

    public static void setFilterFreq(FilterBasic filter, float targetFreq, dur changeTime){
        setFilterFreq(filter,targetFreq,changeTime,1::ms);
    }

    public static void setFilterFreq(FilterBasic filter1, FilterBasic filter2, float targetFreq, dur changeTime){
        setFilterFreq(filter1,targetFreq,changeTime);
        setFilterFreq(filter2,targetFreq,changeTime);
    }


    
    public static void lofi(UGen org, Step resampled, int rate, int bits){
        second / rate => dur re_samp_rate;
    
        //calculate the maximum integer value of the signal's amplitude at this bitdepth
        Math.pow(2, bits) * .5 => float scale;
        //division is expensive and we only need to do it once
        1/scale => float downscale;
        
        while(true)
        {
            //digital clipping
            if (org.last() > 1) 1 => resampled.next;
            else if (org.last() < -1) -1 => resampled.next;
            
            else
            {
                //cast to int for cheep&cheerful rounding
                //then multiply back into the range
                //write result to output
                downscale * ( (org.last() * scale) $ int) => resampled.next;
                
            }
            
            //samplerate is implicid in controll-rate
            re_samp_rate => now;
        }
    }
    

    public static int section(float needle, int nb_sections){
        return section(needle, nb_sections, 0, 1);
    }
    

    public static int section(float needle, int nb_sections, float lo, float hi){
        if(needle <= lo){
            return 0;
        } else
        if(needle >= hi){
            return nb_sections - 1;
        }
        else {
            (needle - lo) / (hi - lo) => needle;
            return (needle * nb_sections) $ int;
        }
        
    }
    


}

Global dummy;

0 => Global.root;
[0,1,2,3,4,5,6,7,8,9,10,11] @=> Global.chrom;
[0,2,4,5,7,9,11] @=> Global.dia;
[0,2,4,7,9] @=> Global.penta;

//0 => Global.chunks;

10::week => now;