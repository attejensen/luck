// name: lib_sporker.ck
// desc: load/unload .ck-files
// author: Atte Jensen

class pattern{
    int id;
    string filename;

    fun void clear(){
        0 => id;
        "" => filename;
    }

    fun void print(){
        <<<"id:",id,"filename:",filename>>>;
    }
}

public class Sporker{
    200 => static int max_patterns;
    0 => static int nb_running_patterns;
    new pattern[max_patterns] @=> static pattern running_patterns[];
    static int i;
    static int id;
    

    public static void toggle(string filename){
        if(is_running(filename)){
            remove(filename);
        }
        else {
            add(filename);
        }
    }
    
    public static void replace(string filename){
        remove(filename);
        add(filename);
    }

    public static int add(string filename){
        if(!is_running(filename)){
            for(0=>i; i<max_patterns; i++){
                if(running_patterns[i].id == 0){
                    Machine.add(filename) => id;
                    if(id != 0){
                        id => running_patterns[i].id;
                        filename => running_patterns[i].filename;
                        nb_running_patterns + 1 => nb_running_patterns;
                    }
                    else {
                        <<<"------------ NOT ABLE TO LOAD FILE",filename,"-----------">>>;
                        //Machine.crash();
                    }
                    max_patterns => i;
                    return id;
                    
                }
            }
        }
        return 0;
    }
    
    public static int remove(string filename){
        false => int success; // fixme, must return false if cant remove
        for(0=>i; i<max_patterns; i++){
            if(running_patterns[i].filename == filename){
                Machine.remove(running_patterns[i].id) => success;
                if(success){
                    nb_running_patterns - 1 => nb_running_patterns;
                    running_patterns[nb_running_patterns].id => running_patterns[i].id;
                    running_patterns[nb_running_patterns].filename => running_patterns[i].filename;
                    running_patterns[nb_running_patterns].clear();
                
                    max_patterns => i;
                }
            }
        }
        return success;
    }

    public static int remove(int index){
        false => int success;
        if(running_patterns[index].filename != ""){
            Machine.remove(running_patterns[index].id) => success;
            if(success){
                nb_running_patterns - 1 => nb_running_patterns;
                running_patterns[nb_running_patterns].id => running_patterns[index].id;
                running_patterns[nb_running_patterns].filename => running_patterns[index].filename;
                running_patterns[nb_running_patterns].clear();
            }
        }
        return success;
    }
    
    public static int is_running(string filename){
        // this wrongfully thinks a patterns is running unless its been
        // removed. however patterns can terminate themselves, so...
        for(0=> i; i<nb_running_patterns; i++){
            if(running_patterns[i].filename == filename){
                return true;
            }
        }
        return false;
    }
}

Sporker dummy;

100::week => now;
