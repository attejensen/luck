public class BCR2000 {
    0 => static int debug;

    new MidiMsg @=> static MidiMsg @ midimessage;
    new MidiOut @=> static MidiOut @ bcr2000_out;
    new MidiIn @=> static MidiIn @ bcr2000_in;

    1 => static int channel;


    connect_by_name("BCR2000 MIDI 1");



    public static void send_globals(){
        send_controller(102,Global.getLength(0,127));
        send_controller(103,Global.getActivity(0,127));
        send_controller(104,Global.getDynamic(0,127));
    }

    
    
    public static void connect_by_name(string port_name){
        int success;

        0 => success;
        for(0=>int i; i<10; i++){
            bcr2000_out.open(i) => success;
            if(bcr2000_out.name() == port_name){
                1 => success;
                //<<<"out:",i>>>;
                break;
            }
        }
        if(!success){
            <<<"no outport named:" + port_name>>>;
        }

        0 => success;
        for(0=>int i; i<10; i++){
            bcr2000_in.open(i) => success;
            if(bcr2000_in.name() == port_name){
                1 => success;
                break;
            }
        }
        if(!success){
            <<<"no inport named:" + port_name>>>;
        }
    }


    public static void send_controller(int controllernum, float value){
        send_controller(controllernum, value $ int);
    }

    public static void send_controller(int controllernum, int value){
	    send_3bytes(0xb, controllernum, value);
    }

    public static void send_3bytes(int command, int byte1, int byte2){

	    ((command & 0xf) << 4) | ((channel - 1) & 0xf) => midimessage.data1;
	    command | channel => command;
	    byte1 & 0x7f  => midimessage.data2;
        if(byte2 != -1){
	        byte2 & 0x7f => midimessage.data3;
        }
        if(debug){
            <<<"Sending midi out...">>>;
            <<<"data1:"+ midimessage.data1>>>;
            <<<"data2:"+ midimessage.data2>>>;
            <<<"data3:"+ midimessage.data3>>>;
        }
	    bcr2000_out.send(midimessage);
        
    }


    
    
    public static void listen(){
        //MidiIn midi_in;
        MidiMsg midi_msg;

        int channel, row;
        int event_channel, event_type;
        
        int type;

        while(true){
            bcr2000_in => now;
            while(bcr2000_in.recv(midi_msg)){
                midi_msg.data1 & 0x0f => event_channel;
                ((midi_msg.data1 & 0xf0) >> 4) => event_type;
                if(event_type == 0xb){
                    (midi_msg.data2-1)/8 => row;
                    (midi_msg.data2-1)%8 => channel;
                    if(debug)<<<"controller:" + midi_msg.data2+ ", row:" + row + ", channel:" + channel>>>;

                    if(row == 12){
                        /*
                        if(Interface.alt[0]){
                            <<<"row12.alt0">>>;
                        }
                        else if(Interface.alt[1]){
                            <<<"row12.alt1">>>;
                        }
                        */

                        if(false){
                            
                        }
                        else {
                            if(debug)<<<"row12 - meta">>>;
                            midi_msg.data3/127. => float value;


                            if(channel == 0)
                            //Global.setFREEMETANAME(value);
                            <<<"unused SUPER knob">>>;
                            else if(channel == 1)
                            Global.setAtonality(value);

                            else if(channel == 2)
                            Global.setHeight(value);

                            else if(channel == 3)
                            Global.setFilter1(value);

                            else if(channel == 4)
                            Global.setFilter2(value);

                            else if(channel == 5)
                            Global.setLength(value);

                            else if(channel == 6)
                            Global.setActivity(value);
                            
                            else if(channel == 7)
                            Global.setDynamic(value);

                            else
                            <<<"lib_midiiin: error, unknown channel">>>;
                            //midi_msg.data3/127. => Global.lower[channel];
                        }
                    }
                    else if(row == 11){
                        /*if(Interface.alt[0]){
                            <<<"row11.alt0 - delay length">>>;
                            //Mixer.setDelay(channel,(midi_msg.data3/8 +1)* Time.beat()/4);
                            //if(debug)<<<Mixer.echo[channel].delay()>>>;
                        }
                        else if(Interface.alt[1]){
                            <<<"row11.alt1">>>;
                        }
                        else {
                            <<<"row11 - delay feedback FIXME: don't use in songs, use SUPER">>>;
                            Mixer.setFeedback(channel,midi_msg.data3/127.);
                            //midi_msg.data3/127. => Global.middle[channel];
                        }*/
                    }
                    else if(row == 10){
                        /*if(Interface.alt[0]){
                            <<<"row10.alt0 - filter Q">>>;
                            //Math.max(midi_msg.data3/127., .1) => Mixer.bpf[channel].Q;
                            //Mixer.setFilterRes(channel,midi_msg.data3/127.);
                        }
                        else if(Interface.alt[1]){
                            <<<"row10.alt1">>>;
                        }
                        else {
                            <<<"row10 - filter freq FIXME: don't use in songs, use SUPER">>>;
                            //Std.mtof(midi_msg.data3/127. * Global.filterRange + Global.filterLow) => Mixer.bpf[channel].freq;
                            Mixer.setFilterPercent(channel,midi_msg.data3/127.);
                            //midi_msg.data3/127. => Global.upper[channel];
                        }*/
                    }
                    else if(row == 8){
                        /*if(Interface.alt[0]){
                            if(midi_msg.data3 == 0){
                                Mixer.disconnectFx("echo",channel);
                            }
                            else {
                                Mixer.connectFx("echo",channel);
                            }
                        }
                        else if(Interface.alt[1]){
                            if(midi_msg.data3 == 0){
                                Mixer.disconnectFx("bpf",channel);
                            }
                            else {
                                Mixer.connectFx("bpf",channel);
                            }
                        }
                        else {
                            midi_msg.data3/127. => Mixer.mute[channel].gain;
                            Csound.mute(channel,Mixer.mute[channel].gain());
                        }
                        */
                    }
                    else if(row == 9){ // sporker...
                        /*
                        <<<midi_msg.data2>>>;

                        if(Global.useNewParts){
                            spork ~ Parts.schedule(channel);
                            Interface.showPart();
                        }
                        else {
                            if(Sporker.add(channel + ".ck")){
                                for(0 => int i; i<8; i++){
                                    if(i != channel){
                                        Sporker.remove(i + ".ck");
                                    }
                                }
                            }
                            Interface.send_sporks();
                        }*/
                    }

                    else if(row == 0){ // knob 1
                        /*
                        if(Interface.alt[0]){
                            midi_msg.data3/127. => Mixer.echo_fb[channel].gain;
                        }
                        else if(Interface.alt[1]){
                            Std.mtof(midi_msg.data3/127. * 60 + 30) => Mixer.bpf[channel].freq;
                        }
                        else {

                            midi_msg.data3/127. => Mixer.track[channel].gain;
                            Csound.gain(channel,Mixer.track[channel].gain());
                        //}
                        //<<<Global.rot1[channel]>>>;
                    /*
                    }
                    else if(row == 4){ // knob 1, push
                        /*
                        midi_msg.data3/127. => Global.press1[channel];
                        if(midi_msg.data3/127. > .5){
                            Mixer.connectFx("echo",channel);
                        }
                        else {
                            <<<Mixer.connectedEcho[channel]>>>;
                            Mixer.disconnectFx("echo",channel);
                            <<<Mixer.connectedEcho[channel]>>>;
                        }
                        */
                    }
                    else if(row == 1){ // knob 2
                        //Global.rot2[channel] * 10000 + 20 => Mixer.bpf[channel].freq;
                    }
                    else if(row == 5){ // knob 2, push
                        /*
                        midi_msg.data3/127. => Global.press2[channel];
                        if(midi_msg.data3/127. > .5){
                            Mixer.connectFx("bpf",channel);
                        }
                        else {
                            Mixer.disconnectFx("bpf",channel);
                        }*/
                    }

                    // --- review knobs ---
                    else if(row==13 && channel == 0){
                        if(midi_msg.data3/127. == 1){
                            /*
                            for(0=>int i; i<4; i++){
                                0 => Interface.alt[i];
                            }
                            1 => Interface.alt[channel];
                            Interface.show("alt0");
                            */
                        }
                        else {
                            /*
                            0 => Interface.alt[channel];
                            Interface.show("main");
                            */
                        }
                    }

                    else if(row==13 && channel == 1){
                        if(midi_msg.data3/127. == 1){
                            /*
                            for(0=>int i; i<4; i++){
                                0 => Interface.alt[i];
                            }
                            1 => Interface.alt[channel];
                            Interface.show("alt1");
                            */
                        }
                        else {
                            /*
                            0 => Interface.alt[channel];
                            Interface.show("main");
                            */
                        }
                    }
                    /* //unused fx
                    else if(row==13 && channel == 2){
                        if(midi_msg.data3/127. == 1){
                            for(0=>int i; i<4; i++){
                                0 => Interface.alt[i];
                            }
                            1 => Interface.alt[channel];
                            Interface.show("alt2");
                        }
                        else {
                            0 => Interface.alt[channel];
                            Interface.show("main");
                        }
                    }
                    else if(row==13 && channel == 3){
                        if(midi_msg.data3/127. == 1){
                            for(0=>int i; i<4; i++){
                                0 => Interface.alt[i];
                            }
                            1 => Interface.alt[channel];
                            Interface.show("alt3");
                        }
                        else {
                            0 => Interface.alt[channel];
                            Interface.show("main");
                        }
                    }
                    */





                    
                    else {

                    }

                }
            }
        }
    }

}

BCR2000 dummy;
//Interface.send_mixer();
//Interface.send_globals();
spork ~ BCR2000.listen();

1::ms => now;
BCR2000.send_globals();
//spork ~ BCR2000.listenKeyboard();

10::week => now;