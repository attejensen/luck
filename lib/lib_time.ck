public class Time {
    120 => static float tempo;
    
    /*
    4 => nb_quarters_per_bar;
    2 => nb_eights_per_quarter;
    2 => nb_sixteen_per_eight;
    2 => nb_thirtytwo_per_sixteen;
    */
    
    200 => static int factor; // this makes sub return larger numbers
    3 * 64 => static float nbSubs;
    
    //new Shred @=> static Shred @ ticker_shred;
    static Shred @ ticker_shred;
    0 => static int running;
    factor * 1024 => static int nb_ticks;

    1 => static int start_time_stick;

    new timeEvent @=> static timeEvent @ tick;
    new timeEvent @=> static timeEvent @ earlytick;
    
    -1 => static int _tick;

    set_meter(1);

    1::samp => now;

    if(start_time_stick){
        start();
    }

    public static void set_meter(float fraction){
        3 * 64 * fraction => nbSubs;
        (factor * 1024. * fraction) $ int => nb_ticks;
    }
    
    public static float get_meter(){
        return nbSubs/ 64. / 3;
    }
    
    public static void toggle(){
        if(running == 0){
            start();
            
        }
        else {
            stop();
            10::ms => now;
        }
    }
    
    public static void start(){
        <<<"starting time">>>;
        if(start_time_stick == 0){
            <<<"cannot start time, stick missing">>>;
            return;
        }
        if(running == 0){
            1 => running;
            spork ~ ticker() @=> ticker_shred;
        }
    }

    public static void stop(){
        0 => running;
        ticker_shred.exit();
    }

    public static void reset(){
        reset(0);
    }

    public static void reset(float bar){
        (bar * nbSubs) $ int => _tick;
    }
    
    public static void ticker(){
        0 => _tick;
        while(true){
            _tick => tick.tick;
            earlytick.broadcast();
            1::ms => now;
            tick.broadcast();
            beat() / 16. / 3.- 1::ms=> now;
            (_tick + 1) % nb_ticks => _tick;
        }
    }

    public static float sub(float div){
        1 / div => div;
        if(div > 1){
            div * get_meter() => div;
        }
        
        return _tick * div / nbSubs;
    }

    /*
    public static void wait_expensive(float bars){
        //<<<bars>>>;
        if(bars <= 0){
            <<<"ERROR, don't call Time.wait() with bars <= 0!">>>;
            <<<"grep -r wait * | grep '1/' | grep -v '\\.)'">>>;
        }

        if(bars<1 && 0){
            bars / get_meter() => bars;
        }
        //<<<bars>>>;
        
        1 / bars => float div;
        tick => now;
        while(sub(div)% 1 != 0){
            //<<<sub(div)>>>;
            tick => now;
        }
    }
    */
    
    public static void wait(float div){
        1 / div => div;
        if(div > 1 && 0){
            div * get_meter() => div;
        }
        tick => now;
        //<<<"sub(div):" + sub(div)>>>;
        while(sub(1/div)% 1 != 0){
        //while(sub(div * get_meter())% 1 != 0){
            tick => now;
            //<<<"sub(div):" + sub(div)>>>;
        }
        //<<<"the wait is over!">>>;
    }

    /*
    public static void earlywait_expensive(float bars){
        if(bars <= 0){
            <<<"ERROR, don't call Time.wait() with bars <= 0!">>>;
            <<<"grep -r wait * | grep '1/' | grep -v '\\.)'">>>;
        }
        if(bars<1 && 0){
            bars / get_meter() => bars;
        }
        1 / bars => float div;
        earlytick => now;
        while(sub(div)% 1 != 0){
            earlytick => now;
        }
    }
    */
    
    public static void earlywait(float div){
        1 / div => div;
        if(div > 1 && 0){
            div * get_meter() => div;
        }
        earlytick => now;
        while(sub(1/div)% 1 != 0){
        //while(sub(div * get_meter())% 1 != 0){
            earlytick => now;
        }
    }
    
    public static dur beat(){
        return 60::second / tempo;
    }

    public static dur beat(float sub){
        return beat() / sub * 4 * get_meter();
    }

    public static void sync(){
        sync(beat());
    }

    public static void sync(dur sync_to){
        sync_to - (now % sync_to) => now;
    }
    
    public static void sync(float beats){
        beat() * beats => dur T;
        sync(T);
    }


    public static void set_tempo(float target_tempo, dur change_time){
        (Std.sgn(target_tempo - tempo) * 2) $ int => int tempo_inc;
        (target_tempo - tempo) $ int / tempo_inc  => int nb_slices;
        change_time / nb_slices => dur slice_length;
      
        for(0 => int i; i < nb_slices; i++){
            tempo_inc +=> tempo;
            slice_length => now;
        }
        target_tempo => tempo;
    }
    
    public static void set_tempo(int target_tempo){
        set_tempo(target_tempo,0::second);
    }
    
    public static float pos(int beats_per_bar){
        beat() * beats_per_bar => dur T;
        <<<"-----">>>;
        //<<<T>>>;
        //<<<beat()>>>;
        <<<T - (now % T)>>>;
        <<<Math.floor(T / (now % T))>>>;
        <<<now % beat()>>>;
        return .1;
    }
    
    public static float pos(){
        return pos(1);
    }
    
}

Time dummy;

//Time.set_tempo(90);
//90 => Time.tempo;
100::week => now;
