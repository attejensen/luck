public class Launchpad {
    1 => static int debug;

    1. => static float schedule_bars;
    true => static int use_schedule_bars;

    0 => static int reveal_mode;
    "localhost" => string hostname;
    8006 => int revealer_port;
    new OscSend @=> static OscSend @ to_revealer;
    to_revealer.setHost(hostname, revealer_port);

    8010 => int to_launchpad_port;
    8011 => int from_launchpad_port;
    new OscRecv @=> static OscRecv @ from_launchpad;
    from_launchpad_port => from_launchpad.port;
    from_launchpad.listen();
    new OscSend @=> static OscSend @ to_launchpad;
    to_launchpad.setHost(hostname,to_launchpad_port);
    
    
    200::ms => static dur blink_time;
    100::ms => static dur short_blink_time;
    
    //5 => static int out_device; // FIXME 1 or 5, must autoset
    //5 => static int in_device; // FIXME 1 or 5, must autoset
    1 => static int default_channel;
    
    new MidiMsg @=> static MidiMsg @ midimessage;
    new MidiOut @=> static MidiOut @ launchpad_out;
    new MidiIn @=> static MidiIn @ launchpad_in;

    0 => static int button_up;
    1 => static int button_down;
    2 => static int button_left;
    3 => static int button_right;
    4 => static int button_session;
    5 => static int button_user1;
    6 => static int button_user2;
    7 => static int button_mixer;

    now => static time schedule_last_tap;
    200::ms => static dur schedule_double_tap_time;
    
    [
    [0,0,0,0,0,0,0,0],
    [0,0,0,0,0,0,0,0],
    [0,0,0,0,0,0,0,0],
    [0,0,0,0,0,0,0,0],
    [0,0,0,0,0,0,0,0],
    [0,0,0,0,0,0,0,0],
    [0,0,0,0,0,0,0,0],
    [0,0,0,0,0,0,0,0]
    ]
    @=> static int grid[][];

    [0,0,0,0,0,0,0,0] @=> static int arrows[];

    [0,0,0,0,-4,0,0,0] @=> static int nav[];

    [
    ["","","","","","","",""],
    ["","","","","","","",""],
    ["","","","","","","",""],
    ["","","","","","","",""],
    ["","","","","","","",""],
    ["","","","","","","",""],
    ["","","","","","","",""],
    ["","","","","","","",""]
    ]
    @=> static string files[][];

    ["","","","","","","",""]
    @=> static string groups[];
    

    [0,0,0,0,0,0,0,0] @=> static int unlinked[];
    
    
    [12,13,14,15,60,29,63,62] @=> static int colors[];
    ["off","red1","red2","red3","green","yellow1","yellow2","yellow3"] @=> static string color_names[];
    /*
    0 = off
    1 = light red
    2 = medium red
    3 = bright red
    4 = green
    5 = dark yellow
    6 = medium yellow
    7 = bright yellow
    */
    


    // ----------------------------------------------------------- //

    clear();
    
    spork ~ osc_listen();
    spork ~ handle_schedules();
    spork ~ blinker();
    spork ~ schedule_button_blinker();
    
    send_nav();
    
    // ----------------------------------------------------------- //


    public static void reveal(int row, int col){
        if(pos_is_valid(row,col) && files[row][col] != ""){
            reveal(files[row][col]);
        }
    }

    public static void reveal(string filename){
        reveal_osc(filename);
    }

    public static void reveal_text(string filename){
        <<<filename>>>;
    }
    
    public static void reveal_osc(string filename){
        to_revealer.startMsg("/luck/reveal", "s");
        filename => to_revealer.addString;
    }
    
    public static void unlink(int cols[]){
        for(0=>int i; i<cols.cap(); i++){
            unlink(cols[i]);
        }
    }

    public static void unlink(int col){
        1 => unlinked[col];
    }
    
    public static void handle_schedules(){
        while(true){
            Time.earlywait(schedule_bars);

            for(0=>int row; row<grid.cap(); row++){
                for(0=>int col; col<grid[row].cap(); col++){
                    if(is_scheduled(row,col)){
                        add_now(row,col);
                    }
                    if(is_unscheduled(row,col)){
                        remove_now(row,col);
                    }
                }
            }
            
        }
    }

    public static int col_has_scheduled(int col){
        for(0=>int row; row<grid.cap(); row++){
            if(grid[row][col] < 0){
                return true;
            }
        }
        return false;
    }


    public static int clear_col_scheduled(int col){
        for(0=>int row; row<grid.cap(); row++){
            if(grid[row][col] < 0){
                set_unscheduled(row,col);
            }
        }
    }

    
    public static int color2launchad(int color){
        if(color_is_valid(color)){
            return colors[color];
        }
    }

    public static int launchpad2color(int color){
        for(0=>int i; i<colors.cap(); i++){
            if(colors[i] == color){
                return i;
            }
        }
    }
    


    public static void switch_schedule_state(){
        !use_schedule_bars => use_schedule_bars;
        send(-1,4,4- (use_schedule_bars * 4));
    }

    
    public static void schedule_button_blinker(){
        int pos;

        while(true){
            Time.wait(schedule_bars/8.);
            ((Time.sub(schedule_bars) % 1) * 8) $ int => pos;
            
            if(pos == 0 && use_schedule_bars) {
                send(-1,4,"green");
            }
            send(pos,8,"yellow");
            short_blink_time => now;
            if(pos == 0 && use_schedule_bars) {
                send(-1,4,"off");
            }
            send(pos,8,"off");
        }
    }
    
    public static void blinker(){
        blinker(blink_time);
    }
    
    public static void blinker(dur blink_time){
        0 => int light;
        while(true){
            for(0=>int row; row<grid.cap(); row++){
                for(0=>int col; col<grid[row].cap(); col++){
                    if(is_blinking(row,col)){
                        if(light){
                            send(row, col, grid[row][col] * -1);
                        }
                        else {
                            send(row, col, 0);
                        }
                    }
                }
            }
            !light => light;
            blink_time => now;
        }
    }

    

    public static void add_file(int row, int col, string file){
        file => files[row][col];
        set_passive(row,col);
    }

    public static void add_files(string files_to_add[][]){
        for(0=>int row; row<files_to_add.cap(); row++){
            for(0=>int col; col<files_to_add[row].cap(); col++){
                if(files_to_add[row][col] != "" && pos_is_valid(row,col)){
                    add_file(row,col,files_to_add[row][col]);
                }
            }
        }
    }



    public static void add_group(int group, string file){
        if(file!="" && group>=0 && group<groups.cap()){
            file => groups[group];
        }
    }

    public static void add_groups(string groups_to_add[]){
        for(0=>int group; group<groups_to_add.cap(); group++){
            if(groups_to_add[group] != "" && group<groups.cap()){
                add_group(group,groups_to_add[group]);
            }
        }
    }

    
    
    public static void chuck(dur dur){
        [
        [0,0,0,0,0,0,0,0],
        [0,0,0,0,0,0,0,0],
        [0,0,0,0,0,0,0,0],
        [0,0,0,0,0,0,0,0],
        [0,0,0,0,0,0,0,0],
        [0,0,0,0,0,0,0,0],
        [0,0,0,0,0,0,0,0],
        [0,0,0,0,0,0,0,0]
        ]
        @=> int org[][];

        [
        [5,5,5,5,4,5,5,5],
        [5,0,0,0,0,4,0,5],
        [4,4,4,4,0,0,4,0],
        [0,0,0,0,0,0,0,4],
        [4,4,4,4,0,0,4,0],
        [0,0,0,0,0,4,0,0],
        [5,0,0,0,4,0,0,5],
        [5,5,5,5,5,5,5,5]
        ]
        @=> int chuck[][];

        
        
        Helpers.array_print(chuck);
        Helpers.array_print(org);
        Helpers.array_print(grid);
        
        Helpers.array_copy(grid,org);
        Helpers.array_print(org);

        set(chuck);
        dur => now;
        <<<"org">>>;
        set(org);
        Helpers.array_print(org);
        
        
    }

    public static void clear(){
        to_launchpad.startMsg("/launchpad, i, i, s");
        0 => to_launchpad.addInt;
        0 => to_launchpad.addInt;
        "clearall" => to_launchpad.addString;
    }



    public static int in_array(string needle, string haystack[]){
        for(0=>int i; i< haystack.cap(); i++){
            if(needle == haystack[i]){
                return true;
            }
        }
        return false;
    }


    
    public static void set(int new_grid[][]){
        for(0=>int i; i<new_grid.cap(); i++){
            for(0=>int j; j<new_grid[i].cap(); j++){
                if(new_grid[i][j] != grid[i][j]){
                    set(i, j, new_grid[i][j]);
                }
            }
        }
    }

    public static void set(int row, int col, int color, int force){
        if(color_is_valid(color) && pos_is_valid(row,col)){
            if(color != grid[row][col] || force){
                color => grid[row][col];
                send(row, col, color);
            }
        }
    }
    
    public static void set(int row, int col, int color){
        set(row, col, color, 0);
    }

    public static void set_unscheduled(int row, int col){
        -1 => grid[row][col];
    }


    public static int is_unscheduled(int row, int col){
        return(-1 == grid[row][col]);
    }

    public static int is_scheduled(int row, int col){
        return(-4 == grid[row][col]);
    }

    public static int is_scheduled(int col){
        for(0=>int row; row<grid.cap(); row++){
            if(is_scheduled(row,col)){
                return true;
            }
        }
        return false;
    }

    public static void set_scheduled(int row, int col){
        -4 => grid[row][col];
    }

    public static void set_active(int row, int col){
        set(row, col, 4);
    }
    
    public static void set_passive(int row, int col){
        set(row, col, 5);
    }
    
    public static void set_blank(int row, int col){
        set(row, col, 0);
    }
    
    public static int color_is_valid(int color){
        return(color >= 0 && color < colors.cap());
    }
        
    
    public static void send(int row, int col, string color){
        to_launchpad.startMsg("/launchpad, i, i, s");
        row => to_launchpad.addInt;
        col => to_launchpad.addInt;
        color => to_launchpad.addString;
    }
    
    public static void send(int row, int col, int color){
        if(color_is_valid(color)){
            to_launchpad.startMsg("/launchpad, i, i, s");
            row => to_launchpad.addInt;
            col => to_launchpad.addInt;
            color_names[color] => to_launchpad.addString;
        }
    }

    public static void send_nav(){
        for(0=>int col; col<nav.cap(); col++){
            send(-1,col,nav[col]);
        }
    }
    
    public static int pos_is_valid(int row, int col){
        return(row_is_valid(row) && col_is_valid(col));
    }

    public static int row_is_valid(int row){
        return(row >= 0 && row <= files.cap());
    }
    
    public static int col_is_valid(int col){
        return (col >= 0 && col < 8);
    }
    
    public static int is_running(int row, int col){
        return(pos_is_valid(row,col) && grid[row][col] == 4);
    }
    
    public static int is_blinking(int row, int col){
        return(pos_is_valid(row,col) && grid[row][col] < 0);
    }





    
    // ----------------------------------------------------------- //

        
    
    public static void schedule(string file){
        for(0=>int row; row<files.cap();row++){
            for(0=>int col; col<files[row].cap();col++){
                if(files[row][col] == file){
                    schedule(row,col);
                    return;
                }
            }
        }
    }
    
    public static void schedule(string files_to_schedule[]){
        for(0=>int file; file<files_to_schedule.cap();file++){
            schedule(files_to_schedule[file]);
        }
    }


    public static void schedule(){
        for(0=>int row; row<grid.cap(); row++){
            for(0=>int col; col<grid[row].cap(); col++){
                schedule(row,col);
            }
        }
    }

    
    // actual schedule
    public static void schedule(int row, int col){
        if(files[row][col]!=""){
            if(use_schedule_bars){
                set_scheduled(row, col);
            }
            else {
                add_now(row,col);
            }
        }
    }



    public static void add_now(string files[]){
        for(0=>int i; i<files.cap(); i++){
            add_now(files[i]);
        }
    }

    public static void add_now(string file){
        for(0=>int row; row<grid.cap(); row++){
            for(0=>int col; col<grid[row].cap(); col++){
                if(files[row][col] == file){
                    add_now(row,col);
                    return;
                }
            }
        }
        
    }

    

    // actual add
    public static void add_now(int row, int col){
        if(files[row][col] == ""){
            <<<"Oops, this shouldn't have happened, but (kinda) handling it anyways...">>>;
            remove_now(row);
        }
        else {
            if(!unlinked[col]){
                for(0=>int i; i<grid.cap();i++){
                    if(files[i][col] != ""){
                        Sporker.remove(files[i][col]);
                        set_passive(i,col);
                    }
                }
            }
            
            if(Sporker.add(files[row][col]) > 0 || Sporker.is_running(files[row][col])){
                set_active(row, col);
            }
            else {
                set_passive(row,col);
            }
        }
    }




    // ----------------------------------------------------------- //
    public static void unschedule(string file){
        for(0=>int row; row<files.cap();row++){
            for(0=>int col; col<files[row].cap();col++){
                if(files[row][col] == file){
                    unschedule(row,col);
                    return;
                    
                }
            }
        }
    }

    public static void unschedule(string files_to_remove[]){
        for(0=>int file; file<files_to_remove.cap();file++){
            unschedule(files_to_remove[file]);
        }
    }
    


    public static void unschedule(int col){
        for(0=>int row; row<grid.cap();row++){
            if(files[row][col] != "" && (is_running(row,col) || is_scheduled(row,col))){
                unschedule(row,col);
            }
        }
    }

    public static void unschedule_except(string dont_unschedule[]){
        for(0=>int row; row<grid.cap();row++){
            for(0=>int col; col<grid[row].cap();col++){
                if(files[row][col] != "" && (is_running(row,col) || is_scheduled(row,col)) && !in_array(files[row][col],dont_unschedule)){
                    unschedule(row, col);
                }
            }
        }
    }
    
    
    public static void unschedule(){
        for(0=>int row; row<grid.cap();row++){
            for(0=>int col; col<grid[row].cap();col++){
                if(files[row][col] != "" && (is_running(row,col) || is_scheduled(row,col))){
                    unschedule(row, col);
                }
            }
        }
    }
    
    public static void remove_now(string files[]){
        remove_now(files, true);
    }


    public static void remove_now(string files[], int use_stopper_file){
        for(0=>int i; i<files.cap(); i++){
            remove_now(files[i], use_stopper_file);
        }
    }
    

    public static void remove_now(string file){
        remove_now(file,true);
    }
    
    public static void remove_now(string file, int use_stopper_file){
        for(0=>int row; row<grid.cap(); row++){
            for(0=>int col; col<grid[row].cap(); col++){
                if(files[row][col] == file){
                    remove_now(row,col,use_stopper_file);
                    return;
                }
            }
        }
        
    }
    
    public static void remove_now(int col){
        for(0=>int row; row<grid.cap(); row++){
            remove_now(row,col);
        }
    }

    public static void remove_now(){
        for(0=>int row; row<grid.cap(); row++){
            for(0=>int col; col<grid[row].cap(); col++){
                if(files[row][col] != ""){
                    remove_now(row,col,true);
                }
            }
        }
    }


    
    // actual unschedule
    public static void unschedule(int row,int col){
        if(files[row][col]!=""){
            if(use_schedule_bars){
                set_unscheduled(row,col);
            }
            else {
                remove_now(row,col);
            }
        }
    }


    public static void remove_now(int row,int col){
        remove_now(row,col,true);
    }
    
    // actual remove
    public static void remove_now(int row, int col, int use_stopper_file){
        if(pos_is_valid(row,col)){
            if(Sporker.remove(files[row][col])){
                set_passive(row, col);
            }
            else {
                set_passive(row,col);
            }
            if(use_stopper_file){
                Machine.add("_" + files[row][col]);
            }
        }
    }




    // ----------------------------------------------------------- //
    

    public static void osc_listen(){
        from_launchpad.event("/launchpad, i, i, i") @=> OscEvent @ oe_in;
        while(true){
            oe_in => now;
            while(oe_in.nextMsg()) {
                oe_in.getInt() => int row;
                oe_in.getInt() => int col;
                oe_in.getInt() => int pressed;
                if(row >= 0){
                    if(pressed){
                        if(col_is_valid(col)){
                            if(reveal_mode){
                                reveal(row,col);
                            }
                            else {
                                // individual
                                //<<<"col:",col,",row:",row,",pressed:",pressed>>>;
                                if(!unlinked[col]){
                                    if(is_running(row,col) || is_scheduled(row,col)){
                                        unschedule(row,col);
                                    } else
                                    if(files[row][col] == ""){
                                        unschedule(col);
                                    }
                                    else {
                                        unschedule(col);
                                        schedule(row,col);
                                    }
                                } else {
                                if(is_scheduled(row,col) || is_running(row,col)){
                                    unschedule(row,col);
                                }
                                else {
                                    schedule(row,col);
                                }
                            }
                        }
                    }
                    else {
                        if(reveal_mode){
                            reveal(groups[row]);
                        }
                        else {
                            // group
                            //<<<"col:",col,",row:",row,",pressed:",pressed>>>;
                            if(groups[row] == ""){
                                unschedule();
                                for(0=>int i; i<grid[0].cap(); i++){
                                    if(files[row][i] != ""){// && !is_running(row,i) && !is_scheduled(row,i)){
                                        schedule(row,i);
                                    }
                                }
                                
                            }
                            else {
                                //un_schedule();
                                Machine.add(groups[row]);
                                //Sporker.add(groups[row]);
                            }
                        }
                    }
                }
            }
            else if(row == -1){
                //<<<"top">>>;
                if(col == button_up){
                    <<<"up,",pressed>>>;
                } else
                if(col == button_down){
                    <<<"down,",pressed>>>;
                } else
                if(col == button_left){
                    <<<"left,",pressed>>>;
                } else
                if(col == button_right){
                    <<<"right,",pressed>>>;
                } else
                if(col == button_session){
                    0 => int double;
                    if(pressed){
                        if(now - schedule_last_tap > schedule_double_tap_time){
                            //<<<"single">>>;
                            switch_schedule_state();
                        }
                        else {
                            //<<<"double">>>;
                            true => double;
                        }
                        now => schedule_last_tap;
                    }
                    else {
                        if(!double){
                            switch_schedule_state();
                            
                        }
                    }
                    
                } else
                if(col == button_user1){
                    <<<"user1,",pressed>>>;
                } else
                if(col == button_user2){
                    <<<"user2,",pressed>>>;
                } else
                if(col == button_mixer){
                    //<<<"mixer,",pressed>>>;
                    if(pressed){
                        1 => reveal_mode;
                    }
                    else {
                        0 => reveal_mode;
                        reveal("");
                    }
                }
                
            }
            
        }
    }
}













}

Launchpad dummy;
1::week => now;