//spork ~ whole();
//spork ~ quarter();
spork ~ eight();

1::week => now;



fun void quarter(){
    while(true){
        Time.wait(1/4.);
        <<<"sub(1/4.):",Time.sub(1/4.)>>>;
    }
}

fun void whole(){
    while(true){
        Time.wait(1);
        <<<"1">>>;
    }
}

fun void eight(){
    while(true){
        Time.wait(1/8.);
        <<<"sub(8):",Time.sub(1/8.)>>>;
    }
}