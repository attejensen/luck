6 => int track;

1000 => int offset;
.1 => float gain;
330000 => int loop;

SndBuf s => Mixer.track[track].left;
SndBuf s2 => Mixer.track[track].right;
s.read("samples/fb_low_bass_fall.wav");
s2.read("samples/fb_low_bass_fall.wav");
//1 => s2.channel;
s.samples() => s.pos => s2.pos;
gain => s.gain => s2.gain;

while(true){
    Time.wait(1);
    if(Time.sub(1) == 0  && Global.part == -1){
        0 => s.pos;
        offset => s2.pos;
    }
    else if(!Global.in_array(Time.sub(1),[1]) || Global.part != -1){
        loop => s.pos;
        loop + offset => s2.pos;
    }
    
}

