"vec2_bass_043" => string instr;
"bass"  => string track;

1 => int debug;
int last_note, note;;
3 => int octave;

[
13,-1,-1,-1,-1,-1,-1,-1,
-1,-1,-1,-1,-1,-1,-1,8,
1,-1,-1,-1,-1,-1,-1,-1,
-1,-1,-1,-1,-1,-1,-1,8
] @=> int notes[];

//Renoise.note_off(instr,track);
int i;
while(true){
    Time.wait(1/16.);
    (Time.sub(1/16.) % notes.cap()) $ int => i;

    if(notes[i] >= 0){
        Renoise.note_off(instr,track,last_note);
        notes[i] + 12 * octave => note;
        if(debug)<<<note>>>;
        Renoise.note_on(instr,track,note);
        note => last_note;
    }
}
