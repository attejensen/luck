"modlysBell" => string instr;
"electronica"  => string track;

0 => int debug;
4 => int octave;

[6,8,9,13] @=> int notes[];
int note;
spork ~ changer();

int nb_notes;

//Renoise.note_off(instr,track);
while(true){
    Time.wait(1/8.);
    if(nb_notes < 1){
        (Global.getActivity(2,6) * Std.rand2f(.5,2)) $ int => nb_notes;
        if(debug)<<<"group of nb notes",nb_notes>>>;
    }
    
    if(true || Std.rand2f(0,1) < Global.getActivity()){
        notes[Std.rand2(0,notes.cap()-1)] + 12*octave=> note;
        if(debug)<<<"playing note",nb_notes>>>;
        Renoise.note_on(instr,track,note,Std.rand2(1,2)*Time.beat()*.5);
        nb_notes--;
    }

    if(nb_notes < 1){
        // rest
        if(debug)<<<"rest">>>;
        Time.beat() * Global.getActivity(4,2) * Std.rand2f(.6,1.3)=> now;
    }
}

fun void changer(){
    while(true){
        Time.earlywait(1/2.);
        if(Time.sub(2) % 4 >= 3){
            if(debug)<<<"C#sus">>>;
            [6,8,13,14] @=> notes;
        } else
        if(Time.sub(1/2.) % 4 < 1){
            if(debug)<<<"F#m">>>;
            [6,8,9,13] @=> notes;
        } else
        if(Time.sub(1/2.) % 2 < 2){
            if(debug)<<<"E/G#">>>;
            [4,6,8,11] @=> notes;
        }
        else {
            if(debug)<<<"A">>>;
            [1,2,4,9,11,13] @=> notes;
        } 
    }
}