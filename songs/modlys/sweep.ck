"modlys_sweep" => string instr;
"sweep"  => string track;

0 => int debug;

1 => int first;
int i;
64 => int early;

while(true){
    Time.wait(1./early);
    (Time.sub(1./early) % (early * 2)) $ int => i;
    if(i == 0 && first){
        if(debug)<<<"first">>>;
        Renoise.note_on(instr,track);
        0 => first;
    } else
    if(i == early * 2 - 1){
        if(debug)<<<"other if">>>;
        Renoise.note_on(instr,track);
    }
}
