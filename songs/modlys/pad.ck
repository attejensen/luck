"pad" => string instr;
"pad"  => string track;

4 => int octave;

[
[[6,2],[9,2],[13,2]],
[[4,6],[8,2],[11,2]],
[      [9,4],[13,4]],
[[-1]],

[[6,2],[9,2],[13,2]],
[[4,6],[8,2],[11,2]],
[      [9,4],[13,4]],
[[-1]],

[[6,2],[9,2],[13,2]],
[[4,4],[8,2],[11,2]],
[      [9,2],[13,2]],
[[6,2],[11,2],[14,2]],

[[6,8],[8,8],[13,8]],
[[-1]],
[[-1]],
[[-1]]
] @=> int notes[][][];

int note, length;

Renoise.note_off(instr,track,[52,54,56,57,59,61,62]);

int i;
while(true){
    Time.wait(1/2.);
    (Time.sub(1/2.) % notes.cap()) $ int => i;
    for(0=> int j; j<notes[i].cap();j++){
        if(notes[i][j].cap() == 1){
            if(notes[i][j][0] != -1){
                //<<<notes[i][j][0]>>>;
            }
        }
        else if(notes[i][j].cap() == 2){
            notes[i][j][0] + 12*octave => note;
            notes[i][j][1] => length;
            //<<<"note + length",note,length>>>;
            spork ~ Renoise.note_on(instr,track,note, Time.beat()*length*.99);
        }
    }
}