#!/bin/bash
#files_raw=`ls $1`
set_govenor performance
lib=../../lib/
if [ ! -d $1 ]; then
    echo error: $1 not a directory
    exit
fi

if [ -z $1 ]; then
    for i in `ls`; do
        if [ -d "$i" ]; then
            echo $i
        fi
    done
    exit
fi

killall chuck-alsa >/dev/null 2>&1
killall chuck-jack >/dev/null 2>&1
killall chuck-alsa >/dev/null 2>&1
killall chuck-jack >/dev/null 2>&1

#for file in $files_raw; do
#    files=$files' '$1/$file
#done

#killall chuck-alsa
#killall chuck-jack

cd $1

#xrns=`grep xrns init.ck | sed 's/=>.*//g' | sed s/\"//g`
#killall renoise
#renoise $xrns > /dev/null &

# poor mans cacheing
cat *.ck $lib/*.ck > /dev/null

chuck-alsa --bufsize64 \
    $lib/lib_global.ck \
    $lib/lib_helpers.ck \
    $lib/lib_time_event.ck \
    $lib/lib_time.ck \
    $lib/lib_sporker.ck \
    $lib/lib_renoise.ck \
    $lib/lib_bcr2000.ck \
    $lib/lib_launchpad.ck \
    init.ck 
    
#    &
#sleep 1
#chuck-alsa + init.ck 

read

killall chuck-alsa >/dev/null 2>&1
killall chuck-jack >/dev/null 2>&1
killall chuck-alsa >/dev/null 2>&1
killall chuck-jack >/dev/null 2>&1
