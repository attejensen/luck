"bass" => string instr => string track;

[
52,-1,-1,-1,
52,-1,-1,-1,
-1,-1,-1,-1,

-1,-1,-1,-1,
-1,-1,59,-1,
64,-1,62,64
] @=> int notes[];

[
.5,-1,-1,-1,
.5,-1,-1,-1,
-1,-1,-1,-1,

-1,-1,-1,-1,
-1,-1,.2,-1,
.2,-1,.2,.2
] @=> float lengths[];


int i, note, last_note;

while(true){
    Time.wait(1/16.);
    (Time.sub(1/16.) % notes.cap()) $ int=> i;

    if((notes[i] => note) > 0){
        Renoise.note_off(instr,track,last_note);
        spork ~ Renoise.note_on(instr, track, note, Time.beat()*lengths[i]*Global.getLength(.2,2));
        note => last_note;
    }
}
