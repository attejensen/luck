"piano" => string instr;
"pno lo" => string track;

4 => int octave;

[
11,-1,-1,-1,
-1,-1,-1,-1,
-1,-1,-1,-1,

13,-1,-1,-1,
-1,-1,-1,-1,
-1,-1,-1,-1,

14,-1,-1,-1,
-1,-1,-1,-1,
-1,-1,-1,-1,

13,-1,-1,-1,
-1,-1,-1,-1,
-1,-1,-1,-1,





14,-1,-1,-1,
-1,-1,-1,-1,
-1,-1,-1,-1,

13,-1,-1,-1,
-1,-1,-1,-1,
-1,-1,-1,-1,

-1,-1,-1,-1,
14,-1,-1,-1,
11,-1,-1,-1,

13,-1,-1,-1,
-1,-1,-1,-1,
-1,-1,-1,-1

] @=> int notes[];

int i, note, last_note;

while(true){
    (Time.sub(1/16.) % notes.cap()) $ int=> i;

    if((notes[i]) > 0){
        notes[i] + 12 * octave => note;
        Renoise.note_off(instr,track,last_note);
        Std.rand2(5,15) * 1::ms => now;
        spork ~ Renoise.note_on(instr, track, note);
        note => last_note;
    }
    Time.wait(1/16.);
}
