"hh_rev" => string instr;
"hh" => string track;

[
.95,.1,.85,.2,
.1,.75,.1,.2
] @=> float gains[];


int i;
while(true){
    Time.wait(1/16.);
    (Time.sub(1/16.) % gains.cap()) $ int=> i;
    if(gains[i] > Std.rand2f(.8,1.2)-Global.getActivity()) {
        Renoise.note_on(instr,track,48,(gains[i] * 127) $ int);
    }
}
