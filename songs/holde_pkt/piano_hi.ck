"piano" => string instr;
"pno lo" => string track;

5 => int octave;

[
-1,-1,-1,-1,
7,-1,-1,-1,
11,-1,-1,-1,

-1,-1,-1,-1,
9,-1,-1,-1,
-1,-1,-1,-1,

-1,-1,-1,-1,
11,-1,-1,-1,
16,-1,-1,-1,

-1,-1,-1,-1,
9,-1,-1,-1,
-1,-1,-1,-1,


-1,-1,-1,-1,
11,-1,-1,-1,
16,-1,-1,-1,

-1,-1,-1,-1,
9,-1,-1,-1,
6,-1,-1,-1,

7,-1,-1,-1,
-1,-1,-1,-1,
-1,-1,-1,-1,

-1,-1,-1,-1,
-1,-1,-1,-1,
-1,-1,-1,-1

] @=> int notes[];

int i, note, last_note;


//0 => i;
while(true){
    Time.wait(1/16.);
    (Time.sub(1/16.) % notes.cap()) $ int=> i;
    if((notes[i]) > 0){
        notes[i] + 12 * octave => note;
        Renoise.note_off(instr, track, last_note);
        Std.rand2(5,40) * 1::ms => now;
        spork ~ Renoise.note_on(instr, track, note);
        note => last_note;
    }
}
