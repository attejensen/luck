"sdrum7" => string instr;
"add sd" => string track;

[
.1, .2, 1., .2,
.9, .1,.3, .9,
.1, .1,.9, .1,

.1, .2, 1., .2,
.9, .1,.3, .1,
.9, .1,.1, .1

] @=> float prob[];

int i;
while(true){
    Time.wait(1/16.);
    (Time.sub(1/16.) % prob.cap()) $ int => i;
    if(prob[i] > Std.rand2f(.8,1.2) - Global.getActivity()){
        Renoise.note_on(instr,track);
    }
}
