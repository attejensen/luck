"808_bd-short" => string instr;
"bd" => string track;

[
.95,.2,.1,.1,
.9,0,0,.3,
.09,.1,0,.6,

0,.2,.1,.1,
.4,0,0,.3,
.09,.1,0,.6
] @=> float gains[];

int i;
while(true){
    Time.wait(1/16.);
    (Time.sub(1/16.) % gains.cap()) $ int=> i;
    if(gains[i] > Std.rand2f(.8,1.2)-Global.getActivity()) {
        Renoise.note_on(instr,track,48,(gains[i] * 127) $ int);
    }
}




