"sergeclick" => string instr;
"clicks" => string track;


int note;

[
5., .1, .9, .1,
7., .2, .1, .8
] @=> float prob[];
int i;
while(true){
    Time.wait(1/16.);
    (Time.sub(1/16.) % prob.cap()) $ int => i;

    if(Std.rand2f(0,prob[i]*2) < Global.getActivity()){
        Renoise.note_on(instr,track,Std.rand2(48,53),127);
        
        // fast afterbeat
        if(Std.rand2f(0,2) < Global.getActivity()){
            Time.wait(1/32.);
            Renoise.note_on(instr,track,Std.rand2(48,53),100);
            Time.wait(1/32.);
            Renoise.note_on(instr,track,Std.rand2(48,53),127);
        }
    }
}

