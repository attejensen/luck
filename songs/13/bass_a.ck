"ext2stereoSubBass" => string instr;
"bass" => string track;

["c4", "g3", "eb3", "d3"] @=> string notes[];
[0,0,0,15] @=> int beats[];

int i, last_note;
Time.wait(4);


while(true){
    Renoise.note_on(instr,track,notes[i]);
    Time.wait(1/4.);
    10::ms => now;
    while((Time.sub(1/8.) % 16) != beats[(i+1)%beats.cap()]){
        Time.wait(1/8.);
    }
    Renoise.note_off(instr,track,notes[i]);
    (i + 1) % notes.cap() => i;
}



