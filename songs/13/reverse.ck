"beats_05-11" => string instr;
"reverse" => string track;

[
0., 0, 0, 0,
0., 0, 1, 0,

0., 0, 0, 0,
0., 0, 0, 0
] @=> float probs[];

int i;
while(true) {
    Time.wait(1/4.);
    
    (Time.sub(1/4.) % probs.cap()) $ int => i;
    if(probs[i] > Global.getActivity(1,0)){
        Renoise.note_on(instr, track);
    }
}