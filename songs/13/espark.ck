"espark1" => string instr;
"espark" => string track;

[
0.,0,0,0, 
0,0,0,0, 
0,0,0,0, 
0,0,0,0, 

1.,0,0,1., 
0,0,1.,0, 
0,0,0,0, 
0,0,0,0
] @=> float probs[];

int i;
[48,49,50,51,52,53,54,55,56] @=> int notes[];
while(true){
    Time.wait(1/16.);
    (Time.sub(1/16.) % probs.cap()) $ int => i;
    if(probs[i] > .5){
        
        Renoise.note_on(instr,track,Helpers.array_random(notes),Time.beat() * .25);
    }
}
