"fast cutter" => string instr;
"perc fast" => string track;

[
1., .5, .5, .1,
.5, .5, 1., .5,
.5, .5, 1., .5,
.5, 1., 1., 1.
] @=> float dyn[];

int i;
while(true){
    Time.wait(1/16.);
    (Time.sub(1/16.) % dyn.cap()) $ int => i;
    Renoise.note_on(instr, track, 48, (dyn[i] * 128) $ int);
}