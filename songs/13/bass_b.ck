"ext2stereoSubBass" => string instr;
"bass" => string track;

[
51, -1, -1, -1,
43, -1, -1, -1,
39, -1, -1, -1,
43, -1, -1, 53
] @=> int notes[];

int i, last_note;
while(true){
    Time.wait(1/2.);
    (Time.sub(1/2.) % notes.cap()) $ int => i;
    if(notes[i] > -1){
        Renoise.note_off(instr, track, last_note);
        notes[i] => last_note;
        Renoise.note_on(instr, track, last_note);
    }

}