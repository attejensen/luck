"hh" => string track => string instr;

[
1., 0, 1., 1,
0, 0, 0, 0,
0, 0, 0, 0,
0, 0, 0, 0,

0, 0, 0, 0,
0, 0, 0, 0,
0, 0, 0, 0,
0, 0, 0, 0
]
@=> float prob[];

int i;
while(true){
    Time.wait(1/16.);
    (Time.sub(1/16.) $ int) % prob.cap() => i;
    //<<<"hh, i:",i>>>;
    if(prob[i] > Global.getActivity(1,0)){
        Renoise.note_on(instr,track,48);
    }
}
