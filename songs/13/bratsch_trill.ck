"cello" => string instr;
"sampled cello" => string track;

int note;

while(true){
    Time.wait(1/8.); 
    Renoise.note_off(instr, track, note);
   (Time.sub(1/8.)%2 *2 ) $ int+ 48 => note;

    Renoise.note_on(instr, track, note);
}