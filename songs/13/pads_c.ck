spork ~ air();
spork ~ sine();
spork ~ sine_lo();

fun void air(){
    "ps air pad 1" => string instr;
    "air" => string track;

    int i;
    ["eb4","f4","a4"] @=> string notes[];
    
    while(true){
        Time.wait(4);
        Renoise.note_off(instr,track,notes);
        Renoise.note_on(instr,track,notes);
    }
}


fun void sine(){
    "ps sine bell 1" => string instr;
    "sine pad" => string track;

    int i;
    ["eb5","f5","a5"] @=> string notes[];
    
    while(true){
        Time.wait(4);
        (Time.sub(2) $ int) % notes.cap() => i;
        Renoise.note_off(instr,track,notes);
        Renoise.note_on(instr,track,notes);
    }
}

fun void sine_lo(){
    "ps sine bell 1" => string instr;
    "sine lo" => string track;

    int i;
    ["eb4","f4","a4"] @=> string notes[];
    
    while(true){
        Time.wait(4);
        Renoise.note_off(instr,track,notes);
        Renoise.note_on(instr,track,notes);
    }
}


1::week => now;