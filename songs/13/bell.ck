"ps synth bell 4" => string instr;
"bell" => string track;

[
"g4", "d4", "", "eb4",
"", "", "", "",
"", "", "", "",
"", "", "", "",

"d4", "bb3", "", "a3",
"", "", "", "",
"", "", "", "",
"", "", "", "",

"d3", "eb3", "", "bb3",
"", "", "", "",
"", "", "", "",
"", "", "", "a3",

"", "", "", "",
"", "", "", "",
"", "", "", "",
"", "", "", ""
] @=> string notes[];

fun void off(int note){
    Time.wait(1/8.);
    Renoise.note_off(instr,track,note);
    //<<<"after off, note",note>>>;
}

fun void on(int note){
    Renoise.note_on(instr,track,note);
    spork ~ off(note);
}


int i;

Time.wait(4);
while(true){
    Renoise.note_on(instr,track,notes[i]);
    Time.wait(1/8.);
    Renoise.note_off(instr,track,notes[i]);
    (i + 1) % notes.cap() => i;
}