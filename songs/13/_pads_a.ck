



spork ~ air();
spork ~ sine();
spork ~ sine_lo();

fun void air(){
    "ps air pad 1" => string instr;
    "air" => string track;

    [48,50,51,53,55,57,58] @=> int notes[];

    Renoise.note_off(instr,track,notes);
}


fun void sine(){
    "ps sine bell 1" => string instr;
    "sine pad" => string track;

    [50,51,52,53,55,57,58,62,63,67] @=> int notes[];

    Renoise.note_off(instr,track,notes);
}

fun void sine_lo(){
    "ps sine bell 1" => string instr;
    "sine lo" => string track;

    [38,39,40,41,43,45,46,50,51,55] @=> int notes[];
    
    Renoise.note_off(instr,track,notes);
}


1::week => now;