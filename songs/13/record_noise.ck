"record_noise" => string instr;
"record noise" => string track;

48 => int lo;
81 => int hi;

int note;
int i;

fun void stutter(int note, int times, float wait){
    for(0=>int i; i<times; i++){
        Renoise.note_on(instr, track, note);
        Time.wait(wait);
        Renoise.note_off(instr, track, note);
    }
}

while(true){
    Time.wait(1/32.);
    (Time.sub(1/32.) % 256) $ int => i;
    
    if(Global.getActivity() >= .5 && (i == 244 || i == 248 || i == 252)){
        Std.rand2(lo,hi) => note;
        spork ~ stutter(note, 4, 1/32.);
    } else
    if(Global.getActivity() >= .1 && (i == 240 || i == 248)){
        Std.rand2(lo,hi) => note;
        spork ~ stutter(note, 8, 1/32.);
    }
}