spork ~ air();
spork ~ sine();
spork ~ sine_lo();

[0,0,0,15] @=> int beats[];

fun void air(){
    "ps air pad 1" => string instr;
    "air" => string track;

    int i;
    [
    ["c4","eb4","g4"],
    ["d4","g4","bb4"],
    ["eb4","g4","bb4"],
    ["d4","f4","a4"]
    ] @=> string notes[][];


    Time.wait(4);
    while(true){
        Renoise.note_on(instr,track,notes[i]);
        Time.wait(1/4.);
        10::ms => now;
        while((Time.sub(1/8.) % 16) != beats[(i+1)%beats.cap()]){
            Time.wait(1/8.);
        }
        Renoise.note_off(instr,track,notes[i]);
        (i + 1) % notes.cap() => i;
    }
}


fun void sine(){
    "ps sine bell 1" => string instr;
    "sine pad" => string track;

    int i;
    [
    ["d5","eb5","g5"],
    ["a4","bb4","d5"],
    ["d4","eb4","g4"],
    ["e4","f4","a4"]
    ] @=> string notes[][];

    Time.wait(4);
    while(true){
        Renoise.note_on(instr,track,notes[i]);
        Time.wait(1/4.);
        10::ms => now;
        while((Time.sub(1/8.) % 16) != beats[(i+1)%beats.cap()]){
            Time.wait(1/8.);
        }
        Renoise.note_off(instr,track,notes[i]);
        (i + 1) % notes.cap() => i;
    }
}

fun void sine_lo(){
    "ps sine bell 1" => string instr;
    "sine lo" => string track;

    int i;
    [
    ["d4","eb4","g4"],
    ["a3","bb3","d4"],
    ["d3","eb3","g3"],
    ["e3","f3","a3"]
    ] @=> string notes[][];

    Time.wait(4);
    while(true){
        Renoise.note_on(instr,track,notes[i]);
        Time.wait(1/4.);
        10::ms => now;
        while((Time.sub(1/8.) % 16) != beats[(i+1)%beats.cap()]){
            Time.wait(1/8.);
        }
        Renoise.note_off(instr,track,notes[i]);
        (i + 1) % notes.cap() => i;
    }
}


1::week => now;