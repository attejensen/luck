spork ~ air();
spork ~ sine();
spork ~ sine_lo();

fun void air(){
    "ps air pad 1" => string instr;
    "air" => string track;

    int i;
    [
    ["eb4","f4","a4"],
    ["d4","g4","bb4"]
    ] @=> string notes[][];
    
    while(true){
        Time.wait(2);
        (Time.sub(2) $ int) % notes.cap() => i;
        Renoise.note_off(instr,track,notes[(i+1)%notes.cap()]);
        Renoise.note_on(instr,track,notes[i]);
    }
}


fun void sine(){
    "ps sine bell 1" => string instr;
    "sine pad" => string track;

    int i;
    [
    ["eb5","f5","a5"],
    ["a4","bb4","d5"],
    ["eb5","f5","a5"],
    ["d5","g5","bb5"]
    ] @=> string notes[][];
    
    while(true){
        Time.wait(2);
        (Time.sub(2) $ int) % notes.cap() => i;
        Renoise.note_off(instr,track,notes[(i+1)%notes.cap()]);
        Renoise.note_off(instr,track,notes[(i+2)%notes.cap()]);
        Renoise.note_off(instr,track,notes[(i+3)%notes.cap()]);
        Renoise.note_on(instr,track,notes[i]);
    }
}

fun void sine_lo(){
    "ps sine bell 1" => string instr;
    "sine lo" => string track;

    int i;
    [
    ["eb4","f4","a4"],
    ["a3","bb3","d4"],
    ["eb4","f4","a4"],
    ["d4","g4","bb4"]
    ] @=> string notes[][];
    
    while(true){
        Time.wait(2);
        (Time.sub(2) $ int) % notes.cap() => i;
        Renoise.note_off(instr,track,notes[(i+1)%notes.cap()]);
        Renoise.note_off(instr,track,notes[(i+2)%notes.cap()]);
        Renoise.note_off(instr,track,notes[(i+3)%notes.cap()]);
        Renoise.note_on(instr,track,notes[i]);
    }
}


1::week => now;