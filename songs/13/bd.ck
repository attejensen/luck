"bd" => string instr;
"main bd" => string track;

[
1.1,0, 0, 0,
0, 0, .6, 0,
.6, 0, 0, 0,
0, 0, 0, 0,

0, 0, .8,0,
0, .8,0, 0,
0, 0, 0, 0,
0, 0,.3, 0

] @=> float probs[];

int i;
while(true){
    Time.wait(1/16.);
    (Time.sub(1/16.) $ int) % probs.cap() => i;
    if(probs[i] > (Std.rand2f(-.2,.2) + Global.getActivity(1,0))){
        Renoise.note_on(instr, track, 48, (128 * probs[i]) $ int);
    }
}