.7 => Mixer.out.gain;

//Time.set_meter(6/4.);
100 * Time.get_meter() => Time.tempo;

//Time.set_meter(6/4.);
//100 => Time.tempo;

1 => int beat_track;

5 => Global.root;

Sporker.add("0.ck");//0
Sporker.add("fodspor.ck");
Sporker.add("beat.ck");

Sporker.add("chip_hi.ck");
Sporker.add("bleeper_pad.ck");
Sporker.add("chip_lo.ck");
Sporker.add("bass.ck");
Sporker.add("pluck.ck");
Sporker.add("sine_arp.ck");
Sporker.add("noise_sweep.ck");

Sporker.add("theramin_helper.ck");
Sporker.add("theramin.ck");


Sporker.add("backing.ck");
Sporker.add("vocal.ck");

MyMidiOut.micronAllNotesOff();
MyMidiOut.micronPgm(4,112); // AAJ warm lead

Mixer.on([0,6]);
//Mixer.on([2]);

Mixer.setDelay(beat_track, Time.beat() * 2);
.9 => Mixer.echo_fb[beat_track].gain;

Interface.show("main");

//100::ms => now;
Time.start();
1::week => now;