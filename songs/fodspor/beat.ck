1 => int track;
1.5 => float gain;

[3,4,5] @=> int random[];

[
new SndBuf,
new SndBuf,
new SndBuf,
new SndBuf,
new SndBuf,
new SndBuf,
new SndBuf,
new SndBuf,
new SndBuf,
new SndBuf,
new SndBuf,
new SndBuf
] @=> SndBuf bc[];

Gain mix => Mixer.track[track];

for(0 => int i; i< bc.cap(); i++){
    bc[i] => mix;
    bc[i].read("samples/bc_" + (i+1) + ".wav");
    bc[i].samples() => bc[i].pos;
    gain => bc[i].gain;
}

spork ~ gainer();

int sub;
int breaks;
int i;

while(true){
    Time.wait(24);
    24 * 4 => breaks;
    (Time.sub(24) $ int) => sub;
    // hh
    if(Global.in_array(sub % 24,[8,10,16])){
        if(Global.getActivity() >= Std.rand2f(0,.5)){
            0 => bc[11].pos;
        }
    }
    // sd
    if(sub % 24 == 12){
        if(Global.getActivity() >= Std.rand2f(0,.5)){
            0 => bc[8].pos;
        }
        
    } else
    // sd breaks
    if (sub % breaks > breaks - 7){
        if(Std.rand2f(0,1) < .7){
            if(Global.getActivity() >= Std.rand2f(0,.5)){
                0 => bc[8].pos;
            }
        }
    }
    // bd
    if(Global.in_array(sub % 48,[0,24,46])){
        if(Global.getActivity() >= Std.rand2f(0,.5)){
            0 => bc[0].pos;
        }
    } else
    // sd add
    if(sub % 24 == 12){
        if(Global.getActivity() >= Std.rand2f(0,.5)){
            0 => bc[2].pos;
        }
    } else
    // busy
    if(Global.in_array(sub%24,[2,3,4,5,6,7,8,9,11,18,19,20,21,23])){
        if(Global.getActivity() >= Std.rand2f(0,.5)){
            Global.array_random(random) => i;
            0 => bc[i].pos;
        }
    }
    
}


fun void gainer(){
    while(true){
        Math.min(Global.getDynamic(.2,1.8),1.2) => mix.gain;
        50::ms => now;
    }
}