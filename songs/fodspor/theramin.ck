SinOsc vibrato => SawOsc saw => LPF filter => ADSR env => Echo eL => Pan2 pL => Mixer.out;
env => Echo eR => Pan2 pR => Mixer.out;

Time.beat() * 4 => eL.max => eR.max;
Time.beat() * .25 * 3 => eL.delay;
eL.delay() * 2 => eR.delay;
.4 => eL.mix;
.2 => eR.mix;
-1 => pL.pan;
1 => pR.pan;

.14 => float volume;

7 => vibrato.freq;
8 => filter.Q;
2 => saw.sync;

env.set(1::ms, 1::ms, 1, 1::ms);

Hid hi;
HidMsg msg;
0 => int device;

4 => int playButton;
2 => int pitchAxis;
3 => int volumeAxis;
0 => int vibratoAxis;
1 => int modulationAxis;

0 => int min_octave;
5 => int max_octave;

6 => int range;


float freqPos;
float filterPos;
float tonalityPos;
float volumePos;
float vibratoPos;
2 => int octavePos; 


if(!hi.openJoystick(device)){
    <<<"joystick not ready, exiting...">>>;
    me.exit();
}
<<< "joystick ready...", "" >>>;

1 => env.keyOff;

while(true){
    set(freqPos,filterPos,tonalityPos,octavePos,volumePos,vibratoPos);
    volume => env.gain;
    hi => now;
    while(hi.recv(msg)){
        //<<<"which",msg.which,"pos",msg.axisPosition>>>;
        if(msg.isAxisMotion()){
            if(msg.which == pitchAxis){
                msg.axisPosition => freqPos;
            }
            else if(msg.which == modulationAxis){
                msg.axisPosition => filterPos;
            }
            else if(msg.which == volumeAxis){
                msg.axisPosition => volumePos;
            }
            else if(msg.which == vibratoAxis){
                msg.axisPosition => vibratoPos;
            }

        }
        else if(msg.isButtonDown()){
            if(msg.which == playButton){
                1 => env.keyOn;
            }
            else if(msg.which >= 0 && msg.which <= 3) {
                msg.which => tonalityPos;
            }
            else if(msg.which == 6 && octavePos < max_octave ){
                octavePos + 1 =>  octavePos;
            }
            else if(msg.which == 7 && octavePos > min_octave){
                octavePos - 1 =>  octavePos;
            }
        }
        else if(msg.isButtonUp()){
            if(msg.which == playButton){
                1 => env.keyOff;
            }
        }
        else {
            <<<"what this shouldn't happen">>>;
        }

    }
}

fun void set(float freqPos, float filterPos, float tonalityPos, int octavePos, float volumePos, float vibratoPos){
    (36 + octavePos * 12) - freqPos * range => float note;
    Std.mtof(quantize(note - Global.root,tonalityPos)) => saw.freq;
    saw.freq() * ( 4 - filterPos * 3) + 50 => filter.freq;
    (1 + volumePos * .8 ) => saw.gain;
    vibratoPos * 10 => vibrato.gain;
}


fun float quantize(float input, int scale[]){
    128 => float best_distance;
    float this_distance;
    0 => float best;
    int try;
    for(0=>int i; i<11; i++)
    for(0=>int j; j<scale.cap(); j++){
        scale[j]+i*12 => try; 
        Std.fabs(try - input) => this_distance;
        if(this_distance < best_distance){
            this_distance => best_distance;
            try => best;
        }
    }
    return best + Global.root;
}

fun float quantize(float input, float tonalityPos){
    float output;
    if(tonalityPos == 0)
    quantize(input,Global.penta) => output;
    else if(tonalityPos == 1)
    quantize(input,Global.dia) => output;
    else if(tonalityPos == 2)
    quantize(input,Global.chrom) => output;
    else if(tonalityPos == 3)
    input => output;
    return output;// + Global.root;
}
