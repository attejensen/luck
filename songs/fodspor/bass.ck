7 => int track;
1 => float gain;
Sampler bass;

Pan2 mix => Mixer.track[track];

bass.load("samples/dub_bass_stereo_db0_loop.wav",13,2);
bass.connect(mix);
bass.set_envelope(1::ms, 2000::ms, 0, 10::ms);
bass.enable_loop();
bass.set_gain(gain);
[
// part 0 - vamp
[
17, -1,-1, -1,-1,-1,
13, -1,-1, -1,-1,-1
],

// part 1 - vers
[
17, -1,-1, -1,-1,-1,
13, -1,-1, 15,-1,-1,
17, -1,-1, -1,-1,-1,
13, -1,-1, -1,-1,-1,

17, -1,-1, -1,-1,-1,
13, -1,-1, 15,-1,-1,
17, -1,-1, -1,-1,-1,
15, -1,-1, -1,-1,-1
],

// part 2 - bridge
[
13,-1,-1, -1,-1,-1,
15,-1,-1, -1,-1,-1,
13,-1,-1, -1,-1,-1,
12,-1,-1, 12,19,24
],

// part 3 - chorus
[
5,-1,-1, -1,-1,-1,
10,-1,-1, 3,-1,-1,

5,-1,-1, -1,-1,-1,
10,-1,-1, 3,-1,-1,

5,-1,-1, -1,-1,-1,
10,-1,-1, 15,-1,13,

12,-1,-1, -1,19,24,
12,-1,-1, -1,19,24
],

// 4 outro
[
12,-1,-1, -1,19,24,
12,-1,-1, -1,19,24
]


] @=> int on[][];

spork ~ gainer();

int i, note, part;
while(true){
    Time.wait(6);
    if(Global.part < on.cap()){
        Global.part => part;
    }

    (Time.sub(6) $ int) % on[part].cap() => i;
    //<<<"i:" + i>>>;
    if(i % 3 == 0){
        bass.off();
    }
    on[part][i] => note;
    if(note >= 0){
        bass.off();
        bass.on(note);
    }
    
}


fun void gainer(){
    while(true){
        Math.min(Global.getDynamic(.2,1.8), 1.2) => mix.gain;
        70::ms => now;
    }
}
