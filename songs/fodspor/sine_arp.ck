3 => int track;
.2 => float gain;

SinOsc s => ADSR e => Echo dl => Mixer.track[track].left;
e => Echo dr => Mixer.track[track].right;

e.set(10::ms, 30::ms, 0, 20::ms);
gain => s.gain;

Time.beat() / 6. * 5 => dl.max => dl.delay;
Time.beat() / 6. * 8 => dr.max => dr.delay;
.4 => dl.mix => dr.mix;


[
0,2,5,7,9,10,
12,14,17,19,21,22,
24,26,29,31,33,34,
36,38,41,43,45,46,
48,50,53,55,57,58,
60,62,65,67,69,70,
72,74,77,79,81,82,
84

] @=> int notes[];

24 => int offset;

spork ~ gainer();

int i;
while(true){
    Time.wait(48);
    -1 => i;
    // bro
    if(Global.part == 2 && (Time.sub(48) $ int) % 192 >= 144) {
        (Time.sub(48) $ int) % 192 - 144 => i;
    } else
    // direct sprok of chorus
    if(Global.part == 5 && (Time.sub(48) $ int) % 192 >= 144) {
        (Time.sub(48) $ int) % 192 - 144 => i;
    } else
    // chorus
    if(Global.part == 3 && (Time.sub(48) $ int) % 384 >= 336) {
        (Time.sub(48) $ int) % 384 - 336 => i;
    } else
    // sus
    if(Global.part == 4 && (Time.sub(48) $ int) % 92 >= 48){
        (Time.sub(48) $ int) % 92 - 48 => i;
    }
    
    if(i >= 0){
        //<<<i>>>;
        on(notes[i % notes.cap()]);
    }
}

fun void on(int note){
    Std.mtof(note + offset) => s.freq;
    //<<<s.freq()>>>;
    e.keyOn();
}


fun void gainer(){
    while(true){
        Math.min(Global.getDynamic(.2,1.8), 1.3) => e.gain;
        60::ms => now;
    }
}