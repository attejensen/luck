2 => int track;
3 => float gain;
.3 => float delay;
1 => float Q;
1 => int useFilter;


LPF fl, fr;
Q => fl.Q => fr.Q;
SndBuf l, r;
Echo dl, dr;

if(useFilter){
    l => fl => dl => Mixer.track[track].left;
    r => fr => dr => Mixer.track[track].right;
    spork ~ filter();
}
else {
    l => dl => Mixer.track[track].left;
    r => dr => Mixer.track[track].right;
}

Time.beat() / 6. * 2 => dl.max => dl.delay;
Time.beat() / 6. * 1 => dr.max => dr.delay;
delay => dl.mix => dr.mix;

l.read("samples/pluck_c5_bc.wav");
r.read("samples/pluck_c5_bc.wav");
1 => r.channel;
l.samples() => l.pos => r.pos;
gain => l.gain => r.gain;

[
[5,7,9,12,16,17],
[5,7,9,12,16,17],
[5,8,10,12,13,17],
[5,7,10,12,13,17],

[5,7,9,12,16,17],
[5,7,9,12,16,17],
[5,8,10,12,13,17],
[5,7,10,12,13,17],

[5,7,9,12,16,17],
[5,7,9,12,16,17],
[5,8,10,12,13,17],
[5,7,10,12,13,17],

[5,7,10,12,14,17],
[5,7,10,12,14,17],
[5,7,10,12,14,17],
[5,7,10,12,14,17]

] @=> int notes[][];


int i, note;
while(true){
    Time.wait(12);
    l.samples() => l.pos => r.pos;
    if(Global.part == 3){
        (Time.sub(2) $ int) % notes.cap() => i;
        if(notes[i][0] != -1){
            notes[i][Std.rand2(0,notes[i].cap()-1)] => note;
            Global.tuneSample(l,12,note);
            Global.tuneSample(r,12,note);
            0 => l.pos => r.pos;
        }
    }
}

fun void filter(){
    while(true){
        Global.getDynamic(400,18000) => fl.freq => fr.freq;
        55::ms => now;
    }
}