0 => int track;

1 => int intro;

[
new SndBuf,
new SndBuf,
new SndBuf,
new SndBuf,
new SndBuf,
new SndBuf,
new SndBuf,
new SndBuf,
new SndBuf,
new SndBuf,
new SndBuf
] @=> SndBuf l[];

[
new SndBuf,
new SndBuf,
new SndBuf,
new SndBuf,
new SndBuf,
new SndBuf,
new SndBuf,
new SndBuf,
new SndBuf,
new SndBuf,
new SndBuf
] @=> SndBuf r[];

l[0].read("samples/fodspor_intro.wav");
l[1].read("samples/fodspor_1a.wav");
l[2].read("samples/fodspor_1b.wav");
l[3].read("samples/fodspor_2a.wav");
l[4].read("samples/fodspor_2b.wav");
l[5].read("samples/fodspor_3a.wav");
l[6].read("samples/fodspor_3b.wav");
l[7].read("samples/fodspor_4a.wav");
l[8].read("samples/fodspor_4b.wav");
l[9].read("samples/fodspor_5a.wav");
l[10].read("samples/fodspor_5b.wav");

r[0].read("samples/fodspor_intro.wav");
r[1].read("samples/fodspor_1a.wav");
r[2].read("samples/fodspor_1b.wav");
r[3].read("samples/fodspor_2a.wav");
r[4].read("samples/fodspor_2b.wav");
r[5].read("samples/fodspor_3a.wav");
r[6].read("samples/fodspor_3b.wav");
r[7].read("samples/fodspor_4a.wav");
r[8].read("samples/fodspor_4b.wav");
r[9].read("samples/fodspor_5a.wav");
r[10].read("samples/fodspor_5b.wav");

for(0 => int i; i < l.cap(); i++){
    l[i] => Mixer.track[track].left;
    r[i] => Mixer.track[track].right;
    1 => r[i].channel;
    l[i].samples() => l[i].pos => r[i].pos;
}




if(intro){
    Time.wait(4);
    0 => l[0].pos => r[0].pos;
    7000::ms => now;
}
//<<<"over">>>;
int i;
while(true){
    Time.wait(4);
    Std.rand2(1,l.cap()-1) => i;
    0 => l[i].pos => r[i].pos;
}
