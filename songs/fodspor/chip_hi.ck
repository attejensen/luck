4 => int track;
3.5 => float gain;

12 => float root;
SndBuf chip_hi => ADSR e => Mixer.track[track];
chip_hi.read("samples/chip32_hi_c4_loop.wav");
1 => chip_hi.loop;

e.set(10::ms,50::ms,.7,100::ms);
e.keyOff();

spork ~ gainer();

while(true){
    Time.wait(12);
    if(Time.sub(12) % 18 == 3){
        Global.tuneSample(chip_hi,root,17);
        e.keyOn();
    } else
    if(Time.sub(12) % 18 == 5){
        Global.tuneSample(chip_hi,root,24);
        e.keyOn();
    } else
    if(Time.sub(12) % 18 == 12){
        Global.tuneSample(chip_hi,root,12);
        e.keyOn();
    } else
    if(Time.sub(12) % 18 == 14){
        Global.tuneSample(chip_hi,root,17);
        e.keyOn();
    }
    else {
        e.keyOff();
    }
    
}

fun void gainer(){
    while(true){
        gain * Math.min(Global.getDynamic(0,2),1.4) => e.gain;
        50::ms => now;
    }
}
