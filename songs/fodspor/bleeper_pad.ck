2 => int track;
10 => float Q;
.8 => float gain;
Sampler bleep;

Pan2 blout;
gain => blout.gain;
blout.left => LPF fl => blackhole;
Step ll => Echo dl => Mixer.track[track].left;

blout.right => LPF fr => blackhole;
Step lr => Echo dr => Mixer.track[track].right;
//blout => Mixer.track[track].left;
//1000 => fl.freq;

Time.beat() * 1.5 => dl.max => dl.delay;
Time.beat() * 1.75 => dr.max => dr.delay;
.3 => dl.mix => dr.mix;


Q => fl.Q;
bleep.load("samples/bleeper_pad_a2_loop.wav",9,8);
bleep.enable_loop();
bleep.set_envelope(200::ms, 1::ms, 1, 200::ms);
bleep.connect(blout);

spork ~ filter_changer();
spork ~ Global.lofi(fl,ll, 12000, 16);
spork ~ Global.lofi(fr,lr, 12000, 16);

[
// part 0 - vamp
[
[5,9,16],
[-1],
[1,5,12],
[-1]
],
// part 1 - verse
[
[5,9,16],
[-1],
[1,5,12],
[3,7,14],
[5,9,16],
[-1],
[1,5,12],
[-1],

[5,9,16],
[-1],
[1,5,12],
[3,7,14],
[5,9,16],
[-1],
[3,7,14],
[-1]
],
// part 2 - bridge
[
[1,5,12],
[-1],
[3,7,14],
[-1],
[1,5,12],
[-1],
[0,5,10,14],
[-1]
],

// part 3 - chorus
[
[5,9,16],
[-1],
[8,10,13,17],
[7,10,13,17],

[5,9,16],
[-1],
[8,10,13,17],
[7,10,13,17],

[5,9,16],
[-1],
[8,10,13,17],
[7,10,13,17],

[0,5,10,14],
[-1],
[-1],
[-1]

],

// part 4 - outro
[
[0,5,10,14],
[-1],
[-1],
[-1]
]



] @=> int notes[][][];


int sub, note, i, part;
while(true){
    Time.wait(2);
    if(Global.part < notes.cap()){
        Global.part => part;
    }
    (Time.sub(2) $ int) % notes[part].cap() => sub;
    //<<<"bleep, i:" + i>>>;
    if(notes[part][sub][0] >= 0){
        bleep.off();
    }
    for(0 => i; i < notes[part][sub].cap(); i++){
        notes[part][sub][i] => note;
        bleep.on(note);
    }
}

fun void filter_changer(){
    while(true){
        Time.wait(12);
        Global.getDynamic(.2,1.8) * Std.rand2(100,1000) => fl.freq;
        Global.getDynamic(.2,1.8) * Std.rand2(100,1000) => fr.freq;
    }
}