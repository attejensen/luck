//set up
1 => float fxGain;

0 => float lisaGain;
1 => int lisaSub;

.005 => float detune;

Time.beat() * 1 => dur delayTimeLeft;
Time.beat() * 1 => dur delayTimeRight;
.4 => float delayGainLeft;
.4 => float delayGainRight;

0 => float reverbMix;

// call it
Global.vocal(Mixer.out, fxGain, lisaGain, lisaSub, detune, delayTimeLeft, delayTimeRight, delayGainLeft, delayGainRight, reverbMix);

