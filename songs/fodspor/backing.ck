6 => int track;

SndBuf ll => Mixer.track[track].left;
SndBuf lr => Mixer.track[track].right;
SndBuf hl => Mixer.track[track].left;
SndBuf hr => Mixer.track[track].right;
SndBuf vl => Mixer.track[track].left;
SndBuf vr => Mixer.track[track].right;
ll.read("samples/backing_voc_lo.wav");
lr.read("samples/backing_voc_lo.wav");
1 => lr.channel;
hl.read("samples/backing_voc_hi.wav");
hr.read("samples/backing_voc_hi.wav");
1 => hr.channel;
vl.read("samples/vocoder_chorus.wav");
vr.read("samples/vocoder_chorus.wav");
1 => vr.channel;

1.5 => ll.gain => lr.gain;
1.2 => hl.gain => hr.gain;

while(true){
    Time.wait(1);
    if(Global.part != 3){
        ll.samples() => ll.pos => lr.pos;
        hl.samples() => hl.pos => hr.pos;
        vl.samples() => vl.pos => vr.pos;
    }

    if(Time.sub(1) % 8 == 0 && Global.part == 3){
        0 => ll.pos => lr.pos;
        0 => hl.pos => hr.pos;
        0 => vl.pos => vr.pos;
    }
}