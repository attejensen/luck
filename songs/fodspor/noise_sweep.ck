3 => int track;
1.2 => float gain;


SndBuf l => Mixer.track[track].left;
SndBuf r => Mixer.track[track].right;
l.read("samples/noise_sweep.wav");
r.read("samples/noise_sweep.wav");
1 => r.channel;
l.samples() => l.pos => r.pos;

spork ~ gainer();

while(true){
    Time.wait(24);
    if(Global.part == 3 && (Time.sub(24) $ int) % 192 == 143){
        0 => l.pos => r.pos;
    }

    if(Global.part == 4 && (Time.sub(24) $ int) % 48 == 47){
        0 => l.pos => r.pos;
    }
    
    
}

fun void gainer(){
    while(true){
        60::ms => now;
        Math.min(Global.getDynamic(.2,1.8), 1.3) * gain => l.gain => r.gain;
    }
}