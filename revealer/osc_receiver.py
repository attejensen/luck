#!/usr/bin/python
# -*- coding: utf-8 -*-
import liblo, os, sys

def update_text(path, args):
    text =  args[0]
    os.system('clear')
    text = text.replace('.ck','').replace('_',' ')
    sys.stdout.write(text)
    sys.stdout.flush()

try:
    server = liblo.Server(8006)
except liblo.ServerError, err:
    print str(err)
    sys.exit()

server.add_method("/luck/reveal", 's', update_text)

os.system('tput civis')
while True:
    server.recv(10)

