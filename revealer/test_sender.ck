// launch with r.ck

// name
"localhost" => string hostname;
8006 => int port;

// check command line

// send object
OscSend xmit;

// aim the transmitter
xmit.setHost( hostname, port );

// infinite time loop
while( true )
{
    xmit.startMsg( "/luck/reveal", "s" );
    "on" => xmit.addString;
    1::second => now;
    xmit.startMsg( "/luck/reveal", "s" );
    "" => xmit.addString;
    1::second => now;
}
